﻿using System.Drawing;

namespace WarehouseProject.Model
{
    public class Empty : Component
    {
        public Empty(Point p)
        {
            Position = p;
        }
    }
}
