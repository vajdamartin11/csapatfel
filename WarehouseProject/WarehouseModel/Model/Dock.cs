﻿using System;
using System.Drawing;

namespace WarehouseProject.Model
{
    public class Dock : Component
    {
        public Boolean IsFree { get; set; }

        public Dock(Point p)
        {
            Position = p;
        }
    }
}
