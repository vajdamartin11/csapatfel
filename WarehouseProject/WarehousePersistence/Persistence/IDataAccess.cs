﻿using System;
using System.Threading.Tasks;

namespace WarehouseProject.Persistence
{
    public interface IDataAccess
    {
        public Boolean CheckWarehouse(Warehouse warehouse);

        Task<Warehouse> ReadFrom(String path);

        Task SaveTo(String path, Warehouse warehouse);

        Task WriteLogFile(SimulationEventArgs e);
    }
}
