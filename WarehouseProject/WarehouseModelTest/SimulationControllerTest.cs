﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using WarehouseProject.Model;
using WarehouseProject.Persistence;

namespace WarehouseModelTest
{

    [TestClass]
    public class SimulationControllerTest
    {
        private SimulationController _model;
        private Warehouse _warehouse;

        private SimulationController _model2;
        private Warehouse _warehouse2;

        private SimulationController _model3;
        private Warehouse _warehouse3;

        [TestInitialize]
        public void Initialize()
        {
            _warehouse = new Warehouse(6, 6);
            _warehouse[0, 0] = FieldType.Pod;
            _warehouse[0, 5] = FieldType.Robot;
            _warehouse[5, 0] = FieldType.Station;
            _warehouse[5, 5] = FieldType.Dock;
            _warehouse.PodContents.Add(new List<Int32>() { 1 });
            _warehouse.RobotIDs.Add(1);
            _warehouse.StationIDs.Add(1);

            _model = new SimulationController();
            _model.NewSimulation(_warehouse);

            _warehouse2 = new Warehouse(8, 8);
            _warehouse2[0, 3] = FieldType.Station;
            _warehouse2.StationIDs.Add(23);
            _warehouse2[1, 5] = FieldType.Station;
            _warehouse2.StationIDs.Add(22);
            _warehouse2[2, 0] = FieldType.Station;
            _warehouse2.StationIDs.Add(15);
            _warehouse2[2, 1] = FieldType.Station;
            _warehouse2.StationIDs.Add(16);
            _warehouse2[2, 2] = FieldType.Station;
            _warehouse2.StationIDs.Add(17);
            _warehouse2[2, 3] = FieldType.Station;
            _warehouse2.StationIDs.Add(18);
            _warehouse2[2, 4] = FieldType.Station;
            _warehouse2.StationIDs.Add(19);
            _warehouse2[2, 5] = FieldType.Station;
            _warehouse2.StationIDs.Add(20);
            _warehouse2[2, 6] = FieldType.Station;
            _warehouse2.StationIDs.Add(21);
            _warehouse2[4, 1] = FieldType.Station;
            _warehouse2.StationIDs.Add(14);
            _warehouse2[4, 2] = FieldType.Station;
            _warehouse2.StationIDs.Add(13);
            _warehouse2[4, 3] = FieldType.Station;
            _warehouse2.StationIDs.Add(12);
            _warehouse2[4, 4] = FieldType.Station;
            _warehouse2.StationIDs.Add(11);
            _warehouse2[4, 5] = FieldType.Station;
            _warehouse2.StationIDs.Add(10);
            _warehouse2[4, 6] = FieldType.Station;
            _warehouse2.StationIDs.Add(9);
            _warehouse2[7, 0] = FieldType.Station;
            _warehouse2.StationIDs.Add(1);
            _warehouse2[0, 0] = FieldType.Pod;
            _warehouse2.PodContents.Add(new List<Int32>() { 1 });
            _warehouse2[1, 0] = FieldType.Pod;
            _warehouse2.PodContents.Add(new List<Int32>() { 1 });
            _warehouse2[0, 2] = FieldType.Robot;
            _warehouse2.RobotIDs.Add(1);
            _warehouse2[4, 7] = FieldType.Dock;

            _model2 = new SimulationController();
            _model2.NewSimulation(_warehouse2);


            _warehouse3 = new Warehouse(5, 5);
            _warehouse3[0, 0] = FieldType.Robot;
            _warehouse3[1, 0] = FieldType.Robot;
            _warehouse3[0, 4] = FieldType.Pod;
            _warehouse3[1, 4] = FieldType.Pod;
            _warehouse3[2, 4] = FieldType.EmptyPod;
            _warehouse3[0, 2] = FieldType.Dock;
            _warehouse3[1, 2] = FieldType.Dock;
            _warehouse3[2, 2] = FieldType.Dock;
            _warehouse3[4, 1] = FieldType.Dock;
            _warehouse3[4, 0] = FieldType.Station;
            _warehouse3.PodContents.Add(new List<Int32>() { 1 });
            _warehouse3.PodContents.Add(new List<Int32>() { 1 });
            _warehouse3.RobotIDs.Add(1);
            _warehouse3.RobotIDs.Add(2);
            _warehouse3.StationIDs.Add(1);

            _model3 = new SimulationController();
            _model3.NewSimulation(_warehouse3);
        }

        [TestMethod]
        public void MainControllerTest()
        {
            Assert.AreEqual(100, _model.Robots[0].Charge);
            Assert.AreEqual(false, _model.Robots[0].HasPod);
            Assert.AreEqual(false, _model.Robots[0].HasWork);
            _model.MainController();
            Assert.AreEqual(99, _model.Robots[0].Charge);
            Assert.AreEqual(false, _model.Robots[0].HasPod);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            for (Int32 i = 0; i < 4; i++)
            {
                _model.MainController();
            }
            Assert.AreEqual(95, _model.Robots[0].Charge);
            Assert.AreEqual(false, _model.Robots[0].HasPod);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            _model.MainController();
            Assert.AreEqual(94, _model.Robots[0].Charge);
            Assert.AreEqual(true, _model.Robots[0].HasPod);
            Assert.AreEqual(false, _model.Robots[0].HasWork);
            Assert.AreEqual(Dir.Left, _model.Robots[0].Direction);
            _model.MainController();
            Assert.AreEqual(93, _model.Robots[0].Charge);
            Assert.AreEqual(true, _model.Robots[0].HasPod);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            Assert.AreEqual(Dir.Down, _model.Robots[0].Direction);
            for (Int32 i = 0; i < 5; i++)
            {
                _model.MainController();
            }
            Assert.AreEqual(88, _model.Robots[0].Charge);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            Assert.AreEqual(1, _model.Robots[0].Pod.Products.Count);
            _model.MainController();
            Assert.AreEqual(88, _model.Robots[0].Charge);
            Assert.AreEqual(false, _model.Robots[0].HasWork);
            Assert.AreEqual(0, _model.Robots[0].Pod.Products.Count);
            _model.MainController();
            Assert.AreEqual(87, _model.Robots[0].Charge);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            Assert.AreEqual(Dir.Right, _model.Robots[0].Direction);
            _model.MainController();
            Assert.AreEqual(86, _model.Robots[0].Charge);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            Assert.AreEqual(Dir.Up, _model.Robots[0].Direction);
            for (Int32 i = 0; i < 5; i++)
            {
                _model.MainController();
            }
            Assert.AreEqual(81, _model.Robots[0].Charge);
            Assert.AreEqual(true, _model.Robots[0].HasWork);
            Assert.AreEqual(Dir.Up, _model.Robots[0].Direction);
            _model.MainController();
            Assert.AreEqual(80, _model.Robots[0].Charge);
            Assert.AreEqual(false, _model.Robots[0].HasWork);
            Assert.AreEqual(false, _model.Robots[0].HasPod);
        }
        [TestMethod]
        public void MainControllerTest2()
        {
            _model2.MainController();
            Assert.AreEqual(99, _model2.Robots[0].Charge);
            for (Int32 i = 0; i < 85; i++) // 86 steps to reach field before charger
            {
                _model2.MainController();
            }
            _model2.MainController();
            _model2.MainController();
            Assert.AreEqual(13, _model2.Robots[0].Charge);
            for (Int32 i = 0; i < 5; i++)
            {
                _model2.MainController();
            }
            Assert.AreEqual(100, _model2.Robots[0].Charge);


        }
        [TestMethod]
        public void TwoRobotsTestWithCollision()
        {
            for (Int32 i = 0; i < 25; i++) 
            {
                _model3.MainController();
            }
            Assert.AreEqual(new Point(4,0), _model3.Robots[0].Path.Last());
            Assert.AreEqual(new Point(3, 0), _model3.Robots[0].Position);
            _model3.MainController();
            
            Assert.AreEqual(new Point(4, 0), _model3.Robots[0].Position);
            Assert.AreEqual(new Point(3, 0), _model3.Robots[1].Position);
            // Collision, waited few seconds
            for (Int32 i = 0; i < 4; i++)
            {
                _model3.MainController();
                Assert.AreEqual(i+1, _model3.Robots[1].WaitedFor);
            }
            _model3.MainController();
            _model3.MainController();
            Assert.AreEqual(new Point(4, 0), _model3.Robots[0].Position);
            Assert.AreEqual(new Point(0, 4), _model3.Robots[1].Path.Last());
            //Robot2 going back with pod
            for (Int32 i = 0; i < 10; i++)
            {
                _model3.MainController();
            }
            Assert.AreEqual(new Point(1, 4), _model3.Robots[0].Path.Last());
            for (Int32 i = 0; i < 15; i++)
            {
                _model3.MainController();
            }
            //Robot2 to station
            Assert.AreEqual(new Point(4, 0), _model3.Robots[1].Path.Last());
        }
    }
}
