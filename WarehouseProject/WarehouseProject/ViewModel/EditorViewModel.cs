﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using WarehouseProject.Persistence;
using System.Windows;
using System.Linq;

namespace WarehouseProject.ViewModel
{
    public enum Mode { Select,Place,Move }
    public enum PlaceType { Empty, Robot, Dock, Pod, Station }


    public class EditorViewModel : ViewModelBase
    {
        private Boolean _isEnabled;
        private Visibility _placeButtons;
        private Visibility _selectButtons;
        private Int32[,] _podIndices;
        private Int32[,] _podWithContentsIndices;
        private Mode _nextMode;
        private Warehouse _warehouse;
        private IDataAccess _dataAccess;
        private PlaceType _placeType;
        private List<Point> _selectedPods;
        private Point _firstPod;
        private Int32 _curPodIndex;
        private Int32 _curPodWithContentsIndex;
        private Int32[] _products;

        #region Properties
        public FieldType SelectedFieldType { get; set; }
        public Int32 ProductNumber { get; set; }

        public Boolean IsSelectedPod { get; set; }
        public Boolean IsSelectedRobot { get; set; }
        public Boolean IsSelectedDock { get; set; }
        public Boolean IsSelectedStation { get; set; }
        public Boolean IsSelectedEmpty { get; set; }
        public Boolean Selected { get; set; }

        public String Products
        {
            set
            {
                try
                {
                    _products = Array.ConvertAll(value.Split(','), Int32.Parse);
                }
                catch (Exception)
                {
                    MessageBox.Show("Hibás termék", "Hiba", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            get { return ""; }
        }

        public Int32 Rows
        {
            get
            {
                return _warehouse.Rows;
            }
            set
            {
                if (_warehouse.Rows != value) { _warehouse.Rows = value; OnPropertyChanged(); }
            }
        }
        public Int32 Cols
        {
            get
            {
                return _warehouse.Cols;
            }
            set
            {
                if (_warehouse.Cols != value) { _warehouse.Cols = value; OnPropertyChanged(); }
            }
        }
        public String NextMode
        {
            get
            {
                if (_nextMode == Mode.Place) return "Kijelölés mód";
                else return "Elhelyezés mód";
            }
            set { }
        }
        public Boolean Enabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if(_isEnabled != value) { _isEnabled = value; }
            }
        }
        public Visibility PlaceButtons
        {
            get
            {
                return _placeButtons;
            }
            set
            {
                if (_placeButtons != value) { _placeButtons = value; }
            }
        }
        public Visibility SelectButtons
        {
            get
            {
                return _selectButtons;
            }
            set
            {
                if (_selectButtons != value) { _selectButtons = value; }
            }
        }
        #endregion

        public ObservableCollection<PlanField> Fields { get; set; }

        #region Commands
        public DelegateCommand CheckCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand HelpCommand { get; private set; }
        public DelegateCommand ResetCommand { get; private set; }
        public DelegateCommand BackCommand { get; private set; }
        public DelegateCommand ToggleSelectCommand { get; private set; }
        public DelegateCommand FieldClickedCommand { get; private set; }
        public DelegateCommand EditProducts { get; private set; }
        public DelegateCommand RelocateCommand { get; private set; }
        public DelegateCommand WrongField { get; private set; }
        public DelegateCommand MoveCommand { get; private set; }
        public DelegateCommand AddProductsCommand { get; private set; }
        public DelegateCommand RemoveProductsCommand { get; private set; }

        public DelegateCommand ProductCloseCommand { get; private set; }
        #endregion

        #region Events
        public event EventHandler PlaceHelpClicked;
        public event EventHandler SelectHelpClicked;
        public event EventHandler SaveClicked;
        public event EventHandler BackClicked;
        public event EventHandler WrongFieldClicked;
        public event EventHandler EditProductClicked;
        public event EventHandler AddProductsClicked;
        public event EventHandler RemoveProductsClicked;
        public event EventHandler ProductCloseClicked;
        #endregion

        public EditorViewModel(IDataAccess dataAccess)
        {
            _isEnabled = true;
            _placeButtons = Visibility.Visible;
            _selectButtons = Visibility.Hidden;
            _nextMode = Mode.Place;
            _dataAccess = dataAccess;
            IsSelectedEmpty = true;

            CheckCommand = new DelegateCommand(o => OnCheckClicked());
            SaveCommand = new DelegateCommand(o => OnSaveClicked());
            HelpCommand = new DelegateCommand(o => OnHelpClicked());
            ResetCommand = new DelegateCommand(o => OnResetClicked());
            BackCommand = new DelegateCommand(o => OnBackClicked());
            ToggleSelectCommand = new DelegateCommand(o => OnToggleSelectClicked());
            MoveCommand = new DelegateCommand(o => OnMoveClicked());
            EditProducts = new DelegateCommand(o => OnEditProduct());
            AddProductsCommand = new DelegateCommand(o => OnAddProductsCommand());
            RemoveProductsCommand = new DelegateCommand(o => OnRemoveProductsCommand());
            ProductCloseCommand = new DelegateCommand(o => OnProductCloseCommand());
        }

        public void SetupNewEditor(Int32 rows, Int32 cols)
        {
            SetupEditor(new Warehouse(rows, cols));
        }

        public void SetupEditor(Warehouse wh)
        {
            _warehouse = wh;
            _nextMode = Mode.Place;
            _podIndices = new Int32[wh.Rows, wh.Cols];
            _podWithContentsIndices = new Int32[wh.Rows, wh.Cols];

            _curPodIndex = 1;
            _curPodWithContentsIndex = 1;
            for (Int32 i = 0; i < wh.Rows; i++)
            {
                for (Int32 j = 0; j < wh.Cols; j++)
                {
                    if (wh[i, j] == FieldType.Pod || wh[i, j] == FieldType.EmptyPod)
                    {
                        _podIndices[i, j] = _curPodIndex;
                        _curPodIndex++;
                    }
                    if (wh[i, j] == FieldType.Pod)
                    {
                        _podWithContentsIndices[i, j] = _curPodWithContentsIndex;
                        _curPodWithContentsIndex++;
                    }
                }
            }

            Fields = new ObservableCollection<PlanField>();
            for (Int32 i = 0; i < wh.Rows; i++)
            {
                for (Int32 j = 0; j < wh.Cols; j++)
                {
                    Fields.Add(new PlanField
                    {
                        Value = 0,
                        X = i,
                        Y = j,
                        Number = i * wh.Cols + j,
                        Text = "",
                        Click = new DelegateCommand(param => Clicked(Convert.ToInt32(param)))
                    });
                }
            }

            UpdateEditor(wh.Rows, wh.Cols);
        }

        public void UpdateEditor(Int32 rows, Int32 cols)
        {
            Int32 _stationIndexer = 0;
            Int32 _robotIndexer = 0;
            Int32 _podIndexer = 0;
            for (Int32 i = 0; i < rows; i++) 
            {
                for (Int32 j = 0; j < cols; j++)
                {
                    switch(_warehouse[i,j])
                    {
                        case FieldType.Empty:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            Fields[i * cols + j].Text = "";
                            break;
                        case FieldType.Robot:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            Fields[i * cols + j].Text = _warehouse.RobotIDs[_robotIndexer++].ToString();
                            break;
                        case FieldType.Dock:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            break;
                        case FieldType.EmptyPod:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            Fields[i * cols + j].Text = "";
                            break;
                        case FieldType.Pod:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            String _text = String.Join(" ", _warehouse.PodContents[_podIndexer++]);
                            Fields[i * cols + j].Text = _text;
                            break;
                        case FieldType.Station:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            Fields[i * cols + j].Text = _warehouse.StationIDs[_stationIndexer++].ToString();
                            break;
                        case FieldType.SelectedPod:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            if(Fields[i * cols + j].Text != "")
                            {
                                String _text2 = String.Join(" ", _warehouse.PodContents[_podIndexer++]);
                                Fields[i * cols + j].Text = _text2;
                            }
                            break;
                        case FieldType.OKField:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            break;
                        case FieldType.SelectedOKField:
                            Fields[i * cols + j].Value = _warehouse[i, j];
                            if(Fields[i * cols + j].Text != "")
                            {
                                String _text2 = String.Join(" ", _warehouse.PodContents[_podIndexer++]);
                                Fields[i * cols + j].Text = _text2;
                            }
                            
                            break;
                    }
                }
            }
        }

        public Warehouse Warehouse()
        {
            return _warehouse;
        }

        private void SetPlaceType()
        {
            if (IsSelectedDock)
            {
                _placeType = PlaceType.Dock;
            }
            else if (IsSelectedEmpty)
            {
                _placeType = PlaceType.Empty;
            }
            else if (IsSelectedRobot)
            {
                _placeType = PlaceType.Robot;
            }
            else if (IsSelectedStation)
            {
                _placeType = PlaceType.Station;
            }
            else if (IsSelectedPod)
            {
                _placeType = PlaceType.Pod;
            }
        }

        private void Clicked(Int32 index)
        {
            if (_nextMode == Mode.Place)
            {
                SetPlaceType();
                switch (_placeType)
                {
                    case PlaceType.Pod:
                        ChangeFieldToPod(index);
                        break;
                    case PlaceType.Robot:
                        ChangeFieldToRobot(index);
                        break;
                    case PlaceType.Dock:
                        ChangeFieldToDock(index);
                        break;
                    case PlaceType.Station:
                        ChangeFieldToStation(index);
                        break;
                    case PlaceType.Empty:
                        ChangeFieldToEmpty(index);
                        break;
                }
                OnPropertyChanged();
                
            }
            else if(_nextMode == Mode.Select)
            {
                SelectPod(index);
                UpdateEditor(_warehouse.Rows,_warehouse.Cols);
            }
            else if(_nextMode == Mode.Move)
            {
                PodMove(index);
                UpdateEditor(_warehouse.Rows, _warehouse.Cols);
                _nextMode = Mode.Select;
            }
        }

        private void ChangeFieldToPod(Int32 index)
        {
            PlanField field = Fields[index];

            if (field.Value == FieldType.Empty)
            {
                field.Value = FieldType.EmptyPod;
                _warehouse[field.X, field.Y] = FieldType.EmptyPod;
                _podIndices[field.X, field.Y] = _curPodIndex++;
                _warehouse.PodCount++;
            }
            else
            {
                OnWrongFieldClicked();
            }
        }

        private void ChangeFieldToRobot(Int32 index)
        {
            PlanField field = Fields[index];
            if (field.Value == FieldType.Empty)
            {
                Int32 _newRobotID = 1;
                Int32 _newRobotIndex = 0;
                if (_warehouse.RobotIDs != null && _warehouse.RobotIDs.Count > 0)
                {
                    for (Int32 i = 1; i <= _warehouse.RobotIDs.Max(); i++)
                    {
                        if (_warehouse.RobotIDs.Contains(i))
                            _newRobotID++;
                        else
                            break;
                    }
                    for (Int32 i = 0; i <= index; i++)
                    {
                        if (Fields[i].Value == FieldType.Robot)
                            _newRobotIndex++;
                    }

                    _warehouse.RobotIDs.Insert(_newRobotIndex, _newRobotID);
                }
                else
                {
                    _warehouse.RobotIDs = new List<Int32>();
                    _warehouse.RobotIDs.Add(_newRobotID);
                }

                field.Value = FieldType.Robot;
                field.Text = _newRobotID.ToString();
                _warehouse[field.X, field.Y] = FieldType.Robot;
            }
            else
            {
                OnWrongFieldClicked();
            }
        }

        private void ChangeFieldToDock(Int32 index)
        {
            PlanField field = Fields[index];

            if (field.Value == FieldType.Empty)
            {
                field.Value = FieldType.Dock;

                _warehouse[field.X, field.Y] = FieldType.Dock;
                _warehouse.DockCount++;
            }
            else
            {
                OnWrongFieldClicked();
            }
        }

        private void ChangeFieldToStation(Int32 index)
        {
            PlanField field = Fields[index];

            if (field.Value == FieldType.Empty)
            {

                Int32 _newStationID = 1;
                Int32 _newStationIndex = 0;
                if (_warehouse.StationIDs != null && _warehouse.StationIDs.Count > 0)
                {

                    for (Int32 i = 1; i <= _warehouse.StationIDs.Max(); i++)
                    {
                        if (_warehouse.StationIDs.Contains(i))
                            _newStationID++;
                        else
                            break;
                    }
                    for (Int32 i = 0; i <= index; i++)
                    {
                        if (Fields[i].Value == FieldType.Station)
                            _newStationIndex++;
                    }
                    _warehouse.StationIDs.Insert(_newStationIndex, _newStationID);
                }
                else
                {
                    _warehouse.StationIDs = new List<Int32>();
                    _warehouse.StationIDs.Add(_newStationID);
                }

                field.Value = FieldType.Station;
                field.Text = _newStationID.ToString();
                _warehouse[field.X, field.Y] = FieldType.Station;
            }
            else
            {
                OnWrongFieldClicked();
            }
        }

        private void ChangeFieldToEmpty(Int32 index)
        {
            PlanField field = Fields[index];
            if (_warehouse[field.X, field.Y] == FieldType.Pod)
            {
                PodToEmpty(index, field);
            }
            else if (_warehouse[field.X, field.Y] == FieldType.Robot)
            {
                RobotToEmpty(index);
            }
            else if (_warehouse[field.X, field.Y] == FieldType.Station)
            {
                StationToEmpty(index);
            }
            else if (_warehouse[field.X, field.Y] == FieldType.EmptyPod)
            {
                _warehouse.PodCount--;
            }
            else if (_warehouse[field.X, field.Y] == FieldType.Dock)
            {
                _warehouse.DockCount--;
            }

            field.Value = 0;
            field.Text = "";
            _warehouse[field.X, field.Y] = FieldType.Empty;
            RefreshPodIndices();
        }

        private void RefreshPodIndices()
        {
            _curPodWithContentsIndex = 1;
            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    if (_warehouse[i, j] == FieldType.Pod)
                    {
                        _podWithContentsIndices[i, j] = _curPodWithContentsIndex;
                        _curPodWithContentsIndex++;
                    }
                }
            }
        }

        private void StationToEmpty(int index)
        {
            Int32 _stationIndex = 0;
            for (Int32 i = 0; i < index; i++)
            {
                if (Fields[i].Value == FieldType.Station)
                {
                    _stationIndex++;
                }
            }
            _warehouse.StationIDs.RemoveAt(_stationIndex);
        }

        private void RobotToEmpty(int index)
        {
            Int32 _robotIndex = 0;
            for (Int32 i = 0; i < index; i++)
            {
                if (Fields[i].Value == FieldType.Robot)
                {
                    _robotIndex++;
                }
            }
            _warehouse.RobotIDs.RemoveAt(_robotIndex);
        }

        private void PodToEmpty(int index, PlanField field)
        {
            Int32 _podIndex = -1;
            for (Int32 i = 0; i <= index; i++)
            {
                if (Fields[i].Value == FieldType.Pod)
                {
                    _podIndex++;
                }
            }
            _podIndices[field.X, field.Y] = 0;
            _podWithContentsIndices[field.X, field.Y] = 0;
            _warehouse.PodCount--;

            _warehouse.PodContents.RemoveAt(_podIndex);
        }

        private void FindOKFields()
        {
            ResetFields();
            GetSelectedPods();

            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    Boolean isOK = true;
                    if (_warehouse[i, j] == FieldType.Empty || _warehouse[i, j] == FieldType.SelectedPod || _warehouse[i, j] == FieldType.SelectedOKField)
                    {
                        for (Int32 k = 0; k < _selectedPods.Count; k++)
                        {
                            if ((i - Convert.ToInt32(_selectedPods[k].X)) >= 0 && (i - Convert.ToInt32(_selectedPods[k].X)) < _warehouse.Rows &&
                                (j - Convert.ToInt32(_selectedPods[k].Y)) >= 0 && (j - Convert.ToInt32(_selectedPods[k].Y)) < _warehouse.Cols)
                            {
                                if ((_warehouse[i - Convert.ToInt32(_selectedPods[k].X), j - Convert.ToInt32(_selectedPods[k].Y)] != FieldType.Empty &&
                                    _warehouse[i - Convert.ToInt32(_selectedPods[k].X), j - Convert.ToInt32(_selectedPods[k].Y)] != FieldType.SelectedPod &&
                                    _warehouse[i - Convert.ToInt32(_selectedPods[k].X), j - Convert.ToInt32(_selectedPods[k].Y)] != FieldType.SelectedOKField))
                                {
                                    isOK = false;
                                }
                            }
                            else
                            {
                                isOK = false;
                            }
                        }
                        if (isOK && _selectedPods.Count != 0)
                        {

                            PlanField field = Fields[i * _warehouse.Cols + j];
                            if (field.Value == FieldType.Empty)
                            {
                                field.Value = FieldType.OKField;

                                _warehouse[field.X, field.Y] = FieldType.OKField;

                            }
                            else if (field.Value == FieldType.SelectedPod)
                            {
                                field.Value = FieldType.SelectedOKField;

                                _warehouse[field.X, field.Y] = FieldType.SelectedOKField;
                            }
                        }
                    }
                }
            }
        }

        private void GetSelectedPods()
        {
            _selectedPods = new List<Point>();
            Boolean first = true;
            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    if (first && _warehouse[i, j] == FieldType.SelectedPod)
                    {
                        _selectedPods.Add(new Point(0, 0));
                        _firstPod = new Point(i, j);
                        first = false;
                    }
                    else if (!first && _warehouse[i, j] == FieldType.SelectedPod)
                    {
                        _selectedPods.Add(new Point(_firstPod.X - i, _firstPod.Y - j));
                    }
                }
            }
        }

        private void ResetFields()
        {
            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    if (_warehouse[i, j] == FieldType.OKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        field1.Value = FieldType.Empty;

                        _warehouse[i, j] = FieldType.Empty;
                    }
                    else if (_warehouse[i, j] == FieldType.SelectedOKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        field1.Value = FieldType.SelectedPod;

                        _warehouse[i, j] = FieldType.SelectedPod;
                    }
                }
            }
        }

        private void SelectPod(Int32 index)
        {
            PlanField field = Fields[index];

            if (field.Value == FieldType.Pod || field.Value == FieldType.EmptyPod)
            {
                field.Value = FieldType.SelectedPod;

                _warehouse[field.X, field.Y] = FieldType.SelectedPod;
                FindOKFields();
            }
            else if (field.Value == FieldType.SelectedPod && field.Text == "")
            {
                field.Value = FieldType.EmptyPod;

                _warehouse[field.X, field.Y] = FieldType.EmptyPod;
                FindOKFields();
            }
            else if (field.Value == FieldType.SelectedPod && field.Text != "")
            {
                field.Value = FieldType.Pod;

                _warehouse[field.X, field.Y] = FieldType.Pod;
                FindOKFields();
            }
            else if (field.Value == FieldType.SelectedOKField && field.Text == "")
            {
                field.Value = FieldType.EmptyPod;

                _warehouse[field.X, field.Y] = FieldType.EmptyPod;
                FindOKFields();
            }
            else if (field.Value == FieldType.SelectedOKField && field.Text != "")
            {
                field.Value = FieldType.Pod;

                _warehouse[field.X, field.Y] = FieldType.Pod;
                FindOKFields();
            }
            if(_selectedPods != null)
            {
                if (_selectedPods.Count > 0) Selected = true;
                else Selected = false;
                OnPropertyChanged("Selected");
            }
        }
        
        private void PodMove(Int32 index)
        {
            PlanField field = Fields[index];

            if(field.Value == FieldType.OKField || field.Value == FieldType.SelectedOKField)
            {
                Int32[,] temp1 = _podIndices;
                Int32[,] temp2 = new Int32[_warehouse.Rows, _warehouse.Cols];
                for (Int32 i = 0; i < _warehouse.Rows; i++)
                {
                    for (Int32 j = 0; j < _warehouse.Cols; j++)
                    {
                        temp2[i, j] = _podWithContentsIndices[i, j];
                    }
                }
                List<List<Int32>> tempCont = new List<List<Int32>>();
                foreach (List<Int32> p in _warehouse.PodContents)
                {
                    List<Int32> s = new List<Int32>();
                    foreach (Int32 n in p)
                    {
                        s.Add(n);
                    }
                    tempCont.Add(s);
                }

                for (Int32 k = 0; k < _selectedPods.Count; k++)
                {
                    if (Fields[(Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X)) * _warehouse.Cols + (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))].Text == "")
                    {
                        MoveEmptyPods(field, temp1, k);
                    }
                    else if (Fields[(Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X)) * _warehouse.Cols + Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y)].Text != "")
                    {
                        MoveNonEmptyPods(field, temp1, temp2, k);
                    }

                }
                _warehouse.PodContents.Clear();
                RefreshFieldsAfterPodMove(tempCont);
                _podWithContentsIndices = new Int32[_warehouse.Rows, _warehouse.Cols];
                RefreshPodIndices();
            }
            _selectedPods.Clear();
        }

        private void RefreshFieldsAfterPodMove(List<List<int>> tempCont)
        {
            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    if (_podWithContentsIndices[i, j] != 0)
                    {
                        _warehouse.PodContents.Add(tempCont[_podWithContentsIndices[i, j] - 1]);
                    }
                    if (_warehouse[i, j] == FieldType.OKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        field1.Value = FieldType.Empty;

                        _warehouse[i, j] = FieldType.Empty;
                    }
                    else if (_warehouse[i, j] == FieldType.SelectedOKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        if (field1.Text == "")
                        {
                            field1.Value = FieldType.EmptyPod;

                            _warehouse[i, j] = FieldType.EmptyPod;
                        }
                        else if (field1.Text != "")
                        {
                            field1.Value = FieldType.Pod;

                            _warehouse[i, j] = FieldType.Pod;
                        }
                    }
                }
            }
        }

        private void MoveNonEmptyPods(PlanField field, int[,] temp1, int[,] temp2, int k)
        {
            _podIndices[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)] = temp1[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))];
            if (_podIndices[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))] == _podIndices[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)])
            {
                if (_firstPod != new Point(field.X, field.Y))
                {
                    _podIndices[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))] = 0;
                }
            }

            _podWithContentsIndices[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)] = temp2[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))];
            if (_podWithContentsIndices[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))] == _podWithContentsIndices[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)])
            {
                if (_firstPod != new Point(field.X, field.Y))
                {
                    _podWithContentsIndices[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))] = 0;
                }
            }

            _warehouse[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)] = FieldType.Pod;
            if (Fields[(Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X)) * _warehouse.Cols + (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))].Value == _warehouse[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))])
            {
                _warehouse[(Convert.ToInt32(_firstPod.X)) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y)) - Convert.ToInt32(_selectedPods[k].Y)] = FieldType.Empty;
                Fields[(Convert.ToInt32(_firstPod.X)) - Convert.ToInt32(_selectedPods[k].X) * _warehouse.Cols + (Convert.ToInt32(_firstPod.Y)) - Convert.ToInt32(_selectedPods[k].Y)].Text = "";
            }
        }

        private void MoveEmptyPods(PlanField field, int[,] temp1, int k)
        {
            _podIndices[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)] = temp1[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))];
            if (_podIndices[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))] == _podIndices[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)])
            {
                _podIndices[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))] = 0;
            }
            _warehouse[field.X - Convert.ToInt32(_selectedPods[k].X), field.Y - Convert.ToInt32(_selectedPods[k].Y)] = FieldType.EmptyPod;
            if (Fields[(Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X)) * _warehouse.Cols + (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))].Value == _warehouse[Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y))])
            {
                _warehouse[(Convert.ToInt32(_firstPod.X)) - Convert.ToInt32(_selectedPods[k].X), (Convert.ToInt32(_firstPod.Y)) - Convert.ToInt32(_selectedPods[k].Y)] = FieldType.Empty;
            }
        }

        public void OnResetClicked()
        {
            foreach(PlanField field in Fields){
                ChangeFieldToEmpty(field.Number);
            }
            _warehouse.StationIDs.Clear();
            _warehouse.RobotIDs.Clear();
            _warehouse.PodContents.Clear();
            Int32 cols = _warehouse.Cols;
            Int32 rows = _warehouse.Rows;
            _warehouse = new Warehouse(rows, cols);
        }

        public void OnHelpClicked()
        {
            if(_nextMode == Mode.Place)
            {
                if (PlaceHelpClicked != null)
                    PlaceHelpClicked(this, EventArgs.Empty);
            }
            else if(_nextMode == Mode.Select)
            {
                if (SelectHelpClicked != null)
                    SelectHelpClicked(this, EventArgs.Empty);
            }
        }

        public void OnToggleSelectClicked()
        {
            if (_nextMode == Mode.Place)
            {
                _nextMode = Mode.Select;
                Enabled = false;
                PlaceButtons = Visibility.Hidden;
                SelectButtons = Visibility.Visible;
                OnPropertyChanged("NextMode");
                OnPropertyChanged("Enabled");
                OnPropertyChanged("SelectButtons");
                OnPropertyChanged("PlaceButtons");
            }
            else
            {
                _nextMode = Mode.Place;
                Enabled = true;
                PlaceButtons = Visibility.Visible;
                SelectButtons = Visibility.Hidden;
                OnPropertyChanged("NextMode");
                OnPropertyChanged("Enabled");
                OnPropertyChanged("SelectButtons");
                OnPropertyChanged("PlaceButtons");
            }
        }

        public void OnMoveClicked()
        {
            _nextMode = Mode.Move;
            Selected = false;
            OnPropertyChanged("Selected");
        }

        public void OnCheckClicked()
        {
            if (!_dataAccess.CheckWarehouse(_warehouse))
            {
                MessageBox.Show("A raktár felépítése nem megfelelő!\nSzükséges:\n - legalább 1 minden mezőtípusból\n - minden termékhez megfelelő állomás\n - minden robot/polc/töltő/rakodó szomszédos üres mezővel", "Hiba", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show("A raktár megfelelő!", "Warehouse", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void OnSaveClicked()
        {
            if (SaveClicked != null)
                SaveClicked(this, EventArgs.Empty);
        }

        public void OnBackClicked()
        {
            if (BackClicked != null)
                BackClicked(this, EventArgs.Empty);
        }

        public void OnWrongFieldClicked()
        {
            if (WrongFieldClicked != null)
                WrongFieldClicked(this, EventArgs.Empty);
        }

        public void OnEditProduct()
        {
            if (EditProductClicked != null)
                EditProductClicked(this, EventArgs.Empty);
        }

        public void OnAddProductsCommand()
        {
            Boolean isOkay = true;
            if (_warehouse.StationIDs.Count == 0)
            {
                isOkay = false;
            }
            else
            {
                for (Int32 i = 0; i < _products.Length; i++)
                {
                    if (_products[i] > _warehouse.StationIDs.Max())
                    {
                        isOkay = false;
                    }
                }
            }
            
            if (isOkay)
            {
                for (Int32 k = 0; k < _selectedPods.Count; k++)
                {
                    PlanField field = Fields[(Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X)) * _warehouse.Cols + Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y)];
                    if (field.Text == "")
                    {
                        AddToEmptyPod(field);
                    }
                    else if (field.Text != "")
                    {
                        AddToNonEmptyPod(field);
                    }
                }
                List<List<Int32>> tempCont = new List<List<Int32>>();
                foreach (List<Int32> p in _warehouse.PodContents)
                {
                    List<Int32> s = new List<Int32>();
                    foreach (Int32 n in p)
                    {
                        s.Add(n);
                    }
                    tempCont.Add(s);
                }
                _warehouse.PodContents.Clear();
                RefreshPodsAfterAddingProduct(tempCont);

                _podWithContentsIndices = new Int32[_warehouse.Rows, _warehouse.Cols];
                RefreshPodIndices();
                _selectedPods.Clear();
                UpdateEditor(_warehouse.Rows, _warehouse.Cols);
            }
            if (AddProductsClicked != null)
                AddProductsClicked(this, EventArgs.Empty);
            Selected = false;
            OnPropertyChanged("Selected");
        }

        private void AddToNonEmptyPod(PlanField field)
        {
            for (Int32 i = 0; i < _products.Length; i++)
            {
                _warehouse[field.X, field.Y] = FieldType.Pod;
                if (!_warehouse.PodContents[_podWithContentsIndices[field.X, field.Y] - 1].Contains(_products[i]))
                {
                    _warehouse.PodContents[_podWithContentsIndices[field.X, field.Y] - 1].Add(_products[i]);
                    _warehouse.PodContents[_podWithContentsIndices[field.X, field.Y] - 1].Sort();
                }
            }
        }

        private void AddToEmptyPod(PlanField field)
        {
            _warehouse[field.X, field.Y] = FieldType.Pod;
            _podWithContentsIndices[field.X, field.Y] = _curPodWithContentsIndex++;

            _warehouse.PodContents.Add(_products.ToList<Int32>());
        }

        private void RefreshPodsAfterAddingProduct(List<List<int>> tempCont)
        {
            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    if (_podWithContentsIndices[i, j] != 0)
                    {
                        _warehouse.PodContents.Add(tempCont[_podWithContentsIndices[i, j] - 1]);
                    }
                    if (_warehouse[i, j] == FieldType.OKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];

                        _warehouse[i, j] = FieldType.Empty;
                    }
                    else if (_warehouse[i, j] == FieldType.SelectedOKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        if (field1.Text == "")
                        {
                            _warehouse[i, j] = FieldType.EmptyPod;
                        }
                        else if (field1.Text != "")
                        {
                            _warehouse[i, j] = FieldType.Pod;
                        }
                    }
                    else if (_warehouse[i, j] == FieldType.SelectedPod)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        if (field1.Text == "")
                        {
                            _warehouse[i, j] = FieldType.EmptyPod;
                        }
                        else if (field1.Text != "")
                        {
                            _warehouse[i, j] = FieldType.Pod;
                        }
                    }
                }
            }
        }

        private void OnRemoveProductsCommand()
        {
            for (Int32 k = 0; k < _selectedPods.Count; k++)
            {
                PlanField field = Fields[(Convert.ToInt32(_firstPod.X) - Convert.ToInt32(_selectedPods[k].X)) * _warehouse.Cols + Convert.ToInt32(_firstPod.Y) - Convert.ToInt32(_selectedPods[k].Y)];
                if (field.Text == "")
                {
                    _warehouse[field.X, field.Y] = FieldType.EmptyPod;
                }
                else if (field.Text != "")
                {
                    RemoveProductFromNonEmptyPod(field);
                }
            }
            List<Int32> delpos = new List<Int32>();
            for (Int32 i = 0; i < _warehouse.PodContents.Count; i++)
            {
                if (_warehouse.PodContents[i].Count == 0)
                {
                    delpos.Add(i);
                }
            }
            if (delpos.Count != 0)
            {
                for (Int32 i = 0; i < delpos.Count; i++)
                {
                    if (i >= 1)
                    {
                        for (Int32 j = 0; j < delpos.Count; j++)
                        { delpos[j]--; }
                    }
                    _warehouse.PodContents.RemoveAt(delpos[i]);
                }
            }
            RefreshPodsAfterRemovingProduct();

            _selectedPods.Clear();
            UpdateEditor(_warehouse.Rows, _warehouse.Cols);
            if (RemoveProductsClicked != null)
                RemoveProductsClicked(this, EventArgs.Empty);
            Selected = false;
            OnPropertyChanged("Selected");
        }

        private void RemoveProductFromNonEmptyPod(PlanField field)
        {
            for (Int32 i = 0; i < _products.Length; i++)
            {
                if (_warehouse.PodContents[_podWithContentsIndices[field.X, field.Y] - 1].Contains(_products[i]))
                {
                    _warehouse.PodContents[_podWithContentsIndices[field.X, field.Y] - 1].Remove(_products[i]);
                    _warehouse[field.X, field.Y] = FieldType.Pod;
                    if (_warehouse.PodContents[_podWithContentsIndices[field.X, field.Y] - 1].Count == 0)
                    {
                        field.Text = "";
                        _warehouse[field.X, field.Y] = FieldType.EmptyPod;
                        _podWithContentsIndices[field.X, field.Y] = 0;
                    }
                }
            }
        }

        private void RefreshPodsAfterRemovingProduct()
        {
            _curPodWithContentsIndex = 1;
            for (Int32 i = 0; i < _warehouse.Rows; i++)
            {
                for (Int32 j = 0; j < _warehouse.Cols; j++)
                {
                    if (_warehouse[i, j] == FieldType.OKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];

                        _warehouse[i, j] = FieldType.Empty;
                    }
                    else if (_warehouse[i, j] == FieldType.SelectedOKField)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        if (field1.Text == "")
                        {
                            _warehouse[i, j] = FieldType.EmptyPod;
                        }
                        else if (field1.Text != "")
                        {
                            _warehouse[i, j] = FieldType.Pod;
                        }
                    }
                    else if (_warehouse[i, j] == FieldType.SelectedPod)
                    {
                        PlanField field1 = Fields[i * _warehouse.Cols + j];
                        if (field1.Text == "")
                        {
                            _warehouse[i, j] = FieldType.EmptyPod;
                        }
                        else if (field1.Text != "")
                        {
                            _warehouse[i, j] = FieldType.Pod;
                        }
                    }
                    if (_warehouse[i, j] == FieldType.Pod)
                    {
                        _podWithContentsIndices[i, j] = _curPodWithContentsIndex;
                        _curPodWithContentsIndex++;
                    }
                }
            }
        }

        public void OnProductCloseCommand()
        {
            if (ProductCloseClicked != null)
                ProductCloseClicked(this, EventArgs.Empty);
        }
    }
}
