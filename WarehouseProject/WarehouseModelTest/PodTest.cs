using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WarehouseProject.Model;

namespace WarehouseModelTest
{
    [TestClass]
    public class PodTest
    {
        private Pod p;

        [TestInitialize]
        public void InitPodTest()
        {
            List<Int32> l = new List<Int32>();
            l.Add(3); l.Add(2);
            p = new Pod(new System.Drawing.Point(1, 1), l);
        }

        [TestMethod]
        public void AddProductTest()
        {
            p.AddProduct(1);
            Assert.AreEqual(3, p.Products.Count);
            Assert.IsTrue(p.Products.Contains(1));
        }

        [TestMethod]
        public void RemoveProductTest()
        {
            p.RemoveProduct(2);
            Assert.AreEqual(1, p.Products.Count);
            Assert.IsFalse(p.Products.Contains(2));
        }
    }
}
