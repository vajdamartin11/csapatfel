﻿using System.Drawing;

namespace WarehouseProject.Model
{
    public class Component
    {
        protected Point _position;

        public Point Position { get { return _position; } set { _position = value; } }
    }
}
