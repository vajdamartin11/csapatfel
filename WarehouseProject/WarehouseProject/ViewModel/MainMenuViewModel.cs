﻿using System;
using System.Threading.Tasks;
using WarehouseProject.Persistence;

namespace WarehouseProject.ViewModel
{
    public class MainMenuViewModel
    {
        private IDataAccess _dataAccess;

        public DelegateCommand EditorCommand { get; private set; }
        public DelegateCommand SimulationCommand { get; private set; }
        public DelegateCommand QuitCommand { get; private set; }

        public EventHandler GoToEditorMenu;
        public EventHandler GoToSimulation;
        public EventHandler<LoadEventArgs> WarehouseOpened;
        public EventHandler QuitClicked;


        public MainMenuViewModel(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;

            EditorCommand = new DelegateCommand(new Action<object>(o => OnGoToEditorMenu()));
            SimulationCommand = new DelegateCommand(new Action<object>(o => OnGoToSimulation()));
            QuitCommand = new DelegateCommand(new Action<object>(o => OnQuitClicked()));
        }

        public Boolean OpenFile(String fname)
        {
            try
            {
                Warehouse wh = Task.Run(() =>
                    _dataAccess.ReadFrom(fname)).Result;
                OnWarehouseOpened(wh);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void OnGoToEditorMenu()
        {
            if (GoToEditorMenu != null)
                GoToEditorMenu(this, EventArgs.Empty);
        }
        private void OnGoToSimulation()
        {
            if (GoToSimulation != null)
                GoToSimulation(this, EventArgs.Empty);
        }
        private void OnWarehouseOpened(Warehouse wh)
        {
            if (WarehouseOpened != null)
                WarehouseOpened(this, new LoadEventArgs(wh));
        }
        private void OnQuitClicked()
        {
            if (QuitClicked != null)
                QuitClicked(this, EventArgs.Empty);
        }
        public async Task<Warehouse> LoadAsync(String path)
        {
            Warehouse _warehouse = await _dataAccess.ReadFrom(path);
            return _warehouse;
        }
    }
}
