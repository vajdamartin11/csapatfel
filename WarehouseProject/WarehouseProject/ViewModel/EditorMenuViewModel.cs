﻿using System;
using WarehouseProject.Persistence;

namespace WarehouseProject.ViewModel
{
    public class EditorMenuViewModel : ViewModelBase
    {
        private IDataAccess _dataAccess;
        private readonly Int32 MIN_SIZE = 3;
        private readonly Int32 MAX_SIZE = 40;

        public Int32 Rows { get; set; }
        public Int32 Cols{ get; set; }

        #region Commands
        public DelegateCommand FromFileCommand { get; private set; }
        public DelegateCommand NewCommand { get; private set; }
        public DelegateCommand SizeDialogOKCommand { get; private set; }
        public DelegateCommand SizeDialogCancelCommand { get; private set; }
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events
        public event EventHandler GoToEditor;
        public event EventHandler GoToSizeDialog;
        public event EventHandler<LoadEventArgs> WarehouseOpened;
        public event EventHandler<Boolean> SizeDialogOKClicked;
        public event EventHandler SizeDialogCancelClicked;
        public event EventHandler BackClicked;
        #endregion

        public EditorMenuViewModel(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;

            FromFileCommand = new DelegateCommand(o => OnGoToEditor());
            NewCommand = new DelegateCommand(o => OnGoToSizeDialog());
            SizeDialogOKCommand = new DelegateCommand(o => OnSizeDialogOKClicked());
            SizeDialogCancelCommand = new DelegateCommand(o => OnSizeDialogCancelClicked());
            BackCommand = new DelegateCommand(o => OnBackClicked());
        }

        public Boolean OpenFile(string fname)
        {
            try
            {
                Warehouse wh = System.Threading.Tasks.Task.Run(() =>
                    _dataAccess.ReadFrom(fname)).Result;
                OnWarehouseOpened(wh);
            }
            catch
            {
                return false;
            }

            return true;
        }

        #region Private event methods
        private void OnGoToEditor()
        {
            if (GoToEditor != null)
                GoToEditor(this, new LoadEventArgs(new Warehouse(5,5)));
        }
        private void OnGoToSizeDialog()
        {
            if (GoToSizeDialog != null)
                GoToSizeDialog(this, EventArgs.Empty);
        }
        private void OnWarehouseOpened(Warehouse wh)
        {
            if (WarehouseOpened != null)
                WarehouseOpened(this, new LoadEventArgs(wh));
        }
        private void OnSizeDialogOKClicked()
        {
            Boolean sizeOK = Rows >= MIN_SIZE && Cols >= MIN_SIZE && Rows <= MAX_SIZE && Cols <= MAX_SIZE;

            if (SizeDialogOKClicked != null)
                SizeDialogOKClicked(this, sizeOK);
        }
        private void OnSizeDialogCancelClicked()
        {
            if (SizeDialogCancelClicked != null)
                SizeDialogCancelClicked(this, EventArgs.Empty);
        }
        private void OnBackClicked()
        {
            if (BackClicked != null)
                BackClicked(this, EventArgs.Empty);
        }
        #endregion
    }
}
