﻿using System;

namespace WarehouseProject.ViewModel
{
    public class PlanRobotData : ViewModelBase
    {
        private Int32 _id;
        private String _data;
        public Int32 Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged();
                }
            }
        }
        public String Data
        {
            get { return _data; }
            set
            {
                if (_data != value)
                {
                    _data = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
