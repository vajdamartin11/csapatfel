﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using WarehouseProject.Model;

namespace WarehouseModelTest
{
    [TestClass]
    public class RobotTest
    {
        private Robot r;

        [TestInitialize]
        public void InitRobot()
        {
            r = new Robot(new Point(1, 1), 1);
        }

        [TestMethod]
        public void TurnLeftTest()
        {
            r.TurnLeft();
            Assert.AreEqual(Dir.Down, r.Direction);
            r.TurnLeft();
            Assert.AreEqual(Dir.Right, r.Direction);
            r.TurnLeft();
            Assert.AreEqual(Dir.Up, r.Direction);
            r.TurnLeft();
            Assert.AreEqual(Dir.Left, r.Direction);
        }

        [TestMethod]
        public void TurnRightTest()
        {
            r.TurnRight();
            Assert.AreEqual(Dir.Up, r.Direction);
            r.TurnRight();
            Assert.AreEqual(Dir.Right, r.Direction);
            r.TurnRight();
            Assert.AreEqual(Dir.Down, r.Direction);
            r.TurnRight();
            Assert.AreEqual(Dir.Left, r.Direction);
        }

        [TestMethod]
        public void GoForwardTest()
        {
            r.GoForward();
            Assert.AreEqual(new Point(1, 0), r.Position);
            r.TurnRight();
            r.GoForward();
            Assert.AreEqual(new Point(0, 0), r.Position);
            r.TurnRight();
            r.GoForward();
            Assert.AreEqual(new Point(0, 1), r.Position);
            r.TurnRight();
            r.GoForward();
            Assert.AreEqual(new Point(1, 1), r.Position);
        }

        [TestMethod]
        public void PodTest()
        {
            Pod p = new Pod(new Point(2, 2));
            r.LiftPod(p);
            Assert.IsTrue(r.HasPod);
            Assert.IsTrue(r.HasPod);
            Assert.IsNotNull(r.Pod);
            r.UnLiftPod();
            Assert.IsFalse(r.HasPod);
            Assert.IsNull(r.Pod);
        }
    }
}
