﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WarehouseProject.Persistence
{
    public class SimulationEventArgs : EventArgs
    {
        public Int32 StepCount { get; set; }
        public List<Int32> EnergyPerRobot { get; set; }

        public SimulationEventArgs(Int32 stepCount, List<Int32> energyPerRobot)
        {
            StepCount = stepCount;
            EnergyPerRobot = energyPerRobot;
        }
        public Int32 TotalEnergy()
        {
            return EnergyPerRobot.Sum();
        }
    }
}
