using System;
using System.Data;
using System.Windows;
using Microsoft.Win32;

using WarehouseProject.ViewModel;
using WarehouseProject.View;
using WarehouseProject.Persistence;
using WarehouseProject.Model;


namespace WarehouseProject
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private SimulationController _model;
        private IDataAccess _dataAccess;
        
        private MainMenuViewModel _mainMenuVM;
        private EditorMenuViewModel _editorMenuVM;
        private SimulationViewModel _simulationVM;
        private EditorViewModel _editorVM;

        private MainWindow _mainMenuView;
        private EditorMenuWindow _editorMenuView;
        private SimulationWindow _simulationView;
        private EditorWindow _editorWindow;
        private SizeDialogWindow _sizeDialog;
        private ProductDialogWindow _productDialog;
        private StatusWindow _statusView;

        public App()
        {
            Startup += App_Startup;
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            _dataAccess = new FileDataAccess();
            _mainMenuVM = new MainMenuViewModel(_dataAccess);
            _mainMenuView = new MainWindow();

            _mainMenuView.DataContext = _mainMenuVM;

            _mainMenuVM.GoToEditorMenu += VM_MainToEditorMenu;
            _mainMenuVM.GoToSimulation += VM_MainToSimulation;
            _mainMenuVM.WarehouseOpened += VM_SimWarehouseOpened;
            _mainMenuVM.QuitClicked += CloseButtonPressed;

            _mainMenuView.Show();
        }

        private void CloseButtonPressed(object sender, EventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("További szép napot!", "Raktár szimulátor", MessageBoxButton.OK, MessageBoxImage.Information);
            if (result == MessageBoxResult.OK) Current.Shutdown();
        }

        private void VM_MainToEditorMenu(object sender, EventArgs e)
        {
            if (_editorMenuView == null)
            {
                _editorMenuView = new EditorMenuWindow();
                _editorMenuVM = new EditorMenuViewModel(_dataAccess);
                _editorMenuView.DataContext = _editorMenuVM;
                
                _editorMenuVM.GoToSizeDialog += VM_EditorMenuToSizeDialog;
                _editorMenuVM.GoToEditor += VM_EditorMenuToEditor;
                _editorMenuVM.WarehouseOpened += VM_EditorWarehouseOpened;
                _editorMenuVM.BackClicked += VM_BackClicked;
            }

            _editorMenuView.Show();
            _mainMenuView.Close();
            _mainMenuView = null;
        }

        private void VM_EditorWarehouseOpened(object sender, LoadEventArgs e)
        {
            if (_editorWindow == null)
            {
                _editorWindow = new EditorWindow();
                _editorVM = new EditorViewModel(_dataAccess);
                _editorWindow.DataContext = _editorVM;

                _editorVM.WrongFieldClicked += VM_WrongFieldClicked;
                _editorVM.BackClicked += VM_BackToMainMenu;
                _editorVM.SaveClicked += VM_SaveClicked;
                _editorVM.EditProductClicked += VM_EditProductClicked;
                _editorVM.PlaceHelpClicked += VM_PlaceHelpClicked;
                _editorVM.SelectHelpClicked += VM_SelectHelpClicked;
            }

            _editorVM.SetupEditor(e.Warehouse);
            _editorWindow.Show();
            _editorMenuView.Close();
            _editorMenuView = null;
        }

        private void VM_EditorMenuToEditor(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Raktárterv betöltése";
            openFileDialog.Filter = "Raktárterv|*.whs";

            if (openFileDialog.ShowDialog() == true)
            {
                if (!_editorMenuVM.OpenFile(openFileDialog.FileName))
                {
                    MessageBox.Show("A kiválasztott fájl betöltése sikertelen volt!", "Sikertelen betöltés",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void VM_EditorMenuToSizeDialog(object sender, EventArgs e)
        {
            if (_sizeDialog == null)
            {
                _sizeDialog = new SizeDialogWindow();
                _sizeDialog.DataContext = _editorMenuVM;

                _editorMenuVM.SizeDialogCancelClicked += VM_SizeDialogCancelClicked;
                _editorMenuVM.SizeDialogOKClicked += VM_SizeDialogOKClicked;
                _sizeDialog.ShowDialog();
            }
            
        }

        private void VM_SizeDialogOKClicked(object sender, Boolean sizeOK)
        {
            if (!sizeOK)
            {
                MessageBox.Show("A megadott méret nem megfelelő. Az elfogadott méretek: 3 és 40 közötti egész számok.",
                    "Helytelen méret", MessageBoxButton.OK, MessageBoxImage.Error); 
            }
            else
            {
                if (_editorWindow == null)
                {
                    _editorWindow = new EditorWindow();
                    _editorVM = new EditorViewModel(_dataAccess);
                    _editorWindow.DataContext = _editorVM;

                    _editorVM.WrongFieldClicked += VM_WrongFieldClicked;
                    _editorVM.BackClicked += VM_BackToMainMenu;
                    _editorVM.SaveClicked += VM_SaveClicked;
                    _editorVM.EditProductClicked += VM_EditProductClicked;
                    _editorVM.PlaceHelpClicked += VM_PlaceHelpClicked;
                    _editorVM.SelectHelpClicked += VM_SelectHelpClicked;
                }
                _editorVM.SetupNewEditor(_editorMenuVM.Rows, _editorMenuVM.Cols);               

                _editorWindow.Show();
                _sizeDialog.Close();
                _editorMenuView.Close();
                _sizeDialog = null;
                _editorMenuView = null;
            }
        }

        private void VM_SizeDialogCancelClicked(object sender, EventArgs e)
        {
            if(_sizeDialog == null)
            {
                _sizeDialog = new SizeDialogWindow();
            }
            _sizeDialog.Close();
            _sizeDialog = null;
        }

        private void VM_MainToSimulation(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog(); 
                openFileDialog.Title = "Raktárterv betöltése";
                openFileDialog.Filter = "Raktárterv|*.whs";

                if (openFileDialog.ShowDialog() == true)
                {
                    if (!_mainMenuVM.OpenFile(openFileDialog.FileName))
                    {
                        MessageBox.Show("A kiválasztott fájl betöltése sikertelen volt!", "Sikertelen betöltés",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Raktár", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void VM_SimWarehouseOpened(object sender, LoadEventArgs e)
        {
            if (!_dataAccess.CheckWarehouse(e.Warehouse))
            {
                MessageBox.Show("A kiválasztott raktár nem helyes! Ellenőrizze a szerkesztőben.", "Helytelen raktár", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (_simulationView == null)
            {
                _simulationView = new SimulationWindow();
                _model = new SimulationController();
                _simulationVM = new SimulationViewModel(_model);
                _statusView = new StatusWindow();
                _simulationView.DataContext = _simulationVM;
                _statusView.DataContext = _simulationVM;

                _simulationVM.QuitClicked += VM_BackToMainMenu;
                _simulationVM.StatusOpenClicked += VM_OpenStatusWindow;
                _model.SimulationOver += Model_SimulationOver;
            }

            _model.NewSimulation(e.Warehouse);
            _simulationVM.UpdateSimulation(e.Warehouse.Rows, e.Warehouse.Cols);

            _simulationView.Show();
            _statusView.Show();
            _mainMenuView.Close();
            _mainMenuView = null;
        }
	
        public void VM_WrongFieldClicked(object sender, EventArgs e)
        {
            MessageBox.Show("A kiválasztott mezőtípust csak üres helyre teheti!", "Szerkesztő", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public async void VM_SaveClicked(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Raktárterv mentése";
                saveFileDialog.Filter = "Raktárterv|*.whs";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        await _dataAccess.SaveTo(saveFileDialog.FileName, _editorVM.Warehouse());
                    }
                    catch (DataException)
                    {
                        MessageBox.Show("Játék mentése sikertelen!\nHibás az elérési út, vagy a könyvtár nem írható.", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
				MessageBox.Show(ex.Message, "Warehouse", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void VM_BackClicked(object sender, EventArgs e)
        {
            _dataAccess = new FileDataAccess();
            _mainMenuVM = new MainMenuViewModel(_dataAccess);
            _mainMenuView = new MainWindow();

            _mainMenuView.DataContext = _mainMenuVM;

            _mainMenuVM.GoToEditorMenu += VM_MainToEditorMenu;
            _mainMenuVM.GoToSimulation += VM_MainToSimulation;
            _mainMenuVM.WarehouseOpened += VM_SimWarehouseOpened;
            _mainMenuVM.QuitClicked += CloseButtonPressed;

            _mainMenuView.Show();
            _editorMenuView.Close();
            _editorMenuView = null;
        }

        public void VM_BackToMainMenu(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            if (_editorWindow != null)
                msg = "Biztosan ki akar lépni?\nA nem mentett változtatások elveszhetnek!";

            MessageBoxResult result = MessageBox.Show(msg, "Szerkesztő", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                _dataAccess = new FileDataAccess();
                _mainMenuVM = new MainMenuViewModel(_dataAccess);
                _mainMenuView = new MainWindow();

                _mainMenuView.DataContext = _mainMenuVM;

                _mainMenuVM.GoToEditorMenu += VM_MainToEditorMenu;
                _mainMenuVM.GoToSimulation += VM_MainToSimulation;
                _mainMenuVM.WarehouseOpened += VM_SimWarehouseOpened;
                _mainMenuVM.QuitClicked += CloseButtonPressed;

                _mainMenuView.Show();

                if (_editorWindow != null)
                {
                    _editorWindow.Close();
                    _editorWindow = null;
                }
                else if (_simulationView != null)
                {
                    _simulationView.Close();
                    _statusView.Close();
                    _simulationView = null;
                    _statusView = null;
                }
            }
            else return;
        }

        public void VM_OpenStatusWindow(object sender, EventArgs e)
        {
            if (!_statusView.IsVisible)
            {
                _statusView = null;
                _statusView = new StatusWindow();
                _statusView.DataContext = _simulationVM;
                _statusView.Show();
            }
        }

        public void VM_EditProductClicked(object sender, EventArgs e)
        {
            if (_productDialog == null)
            {
                _productDialog = new ProductDialogWindow();
                _productDialog.DataContext = _editorVM;

                _editorVM.AddProductsClicked += VM_ProductDialogButtonsClicked;
                _editorVM.RemoveProductsClicked += VM_ProductDialogButtonsClicked;
                _editorVM.ProductCloseClicked += VM_ProductDialogButtonsClicked;
                _productDialog.Closed += VM_ProductDialogButtonsClicked;
            }
            _productDialog.ShowDialog();
        }

        public void VM_ProductDialogButtonsClicked(object sender, EventArgs e)
        {
            if(_productDialog == null)
            {
                _productDialog = new ProductDialogWindow();
            }
            _productDialog.Close();
            _productDialog = null;
        }

        public void VM_PlaceHelpClicked(object sender, EventArgs e)
        {
            MessageBox.Show("Menü:\n• A 'Fájl' menüben lehetősége van raktára mentésére, valamint kiléphet a szerkesztőből.\n• A 'Szerkesztő módok' menüben válthat át másik módra.\n\n" +
                "Elhelyezés mód:\n• A rádiógombokkal tudja változtatni a mezőtípust.\n• Egy mezőre kattintva megváltoztathatja azt az aktív típusra.\n• Az 'Ellenőrzés' gombra kattintva megvizsgálhatja helyes-e a raktár felépítése.\n" +
                "• A 'Töröl' gombra kattintva törölheti a teljes raktárat (mezőket, termékeket).", "Súgó", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void VM_SelectHelpClicked(object sender, EventArgs e)
        {
            MessageBox.Show("Menü:\n• A 'Fájl' menüben lehetősége van raktára mentésére, valamint kiléphet a szerkesztőből.\n• A 'Szerkesztő módok' menüben válthat át másik módra.\n\n" +
                "Kijelölés mód:\n• A rádiógombok nem aktívak, ebben a módban nincs rájuk szükség.\n• Egy polcra (szürke mező, amennyiben tett le korábban) kattintva kijelölheti azt.\n" +
                "• A kijelölt polc(ok) barna kerettel látható(k).\n• A 'Termék módosít' gombra kattintva a kijelölt polcokon:\n\t- egy/több terméket helyezhetünk el vesszővel elválasztva\n\t- egy/több terméket törölhetünk\n\t- megszakíthatjuk a műveletet\n" +
                "Megjegyzés: Csak olyan számú terméket tehetünk fel a polcra, amihez tartozik lerakóhely, továbbá minden termékből polconként maximum 1 db lehet.\n\n" +
                "• Az áthelyezés gombra kattintva aktiváljuk az áthelyezés módot. A kijelölt polcokat áthelyezhetjük az egyik lilás színnel jelölt mezőre kattintva\n" +
                "Megjegyzés: A kiválasztott mezőre a kijelölt polcok közül a legfelső sor legbaloldalibb polca kerül, a többi ehhez viszonyítva kerül a helyére tartva az eredeti alakzatot.", "Súgó", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Model_SimulationOver(object sender, SimulationEventArgs e)
        {
            _dataAccess.WriteLogFile(e);
            MessageBox.Show("A szimuláció befejeződött!\nA naplófájl írása befejeződött.", "Szimuláció vége", MessageBoxButton.OK);
        }
    }
}
