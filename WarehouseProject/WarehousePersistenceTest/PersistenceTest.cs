using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarehouseProject.Persistence;
using Moq;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WarehousePersistenceTest
{
    [TestClass]
    public class PersistenceTest
    {
        private Mock<IDataAccess> _mock;
        private Warehouse _warehouse;
        private IDataAccess _dataaccessLoad;
        private IDataAccess _dataaccessTests;

        [TestInitialize]
        public void Init()
        {
            _warehouse = new Warehouse(4, 4);
            _warehouse[0, 0] = FieldType.Robot;
            _warehouse[0, 1] = FieldType.Robot;
            _warehouse[1, 2] = FieldType.Pod;
            _warehouse[3, 0] = FieldType.Dock;
            _warehouse[3, 3] = FieldType.Station;
            _warehouse.PodContents.Add(new List<Int32>() { 1 });
            _warehouse.RobotIDs.Add(1);
            _warehouse.RobotIDs.Add(2);

            _mock = new Mock<IDataAccess>();
            _mock.Setup(mock => mock.ReadFrom(It.IsAny<string>()))
                .Returns(() => Task.FromResult(_warehouse));

            _dataaccessLoad = _mock.Object;
            _dataaccessTests = new FileDataAccess();
        }

        [TestMethod]
        public async Task ReadFromTest()
        {
            Warehouse _wh = new Warehouse(5, 5);
            Assert.AreEqual(_wh.Rows, 5);
            Assert.AreEqual(_wh.Cols, 5);
            Assert.AreEqual(_wh.PodContents.Count, 0);
            Assert.AreEqual(_wh.RobotIDs.Count, 0);
            for (Int32 i = 0; i < 5; i++)
                for (Int32 j = 0; j < 5; j++)
                {
                    Assert.AreEqual(_wh[i, j], FieldType.Empty);
                }

            _wh = Task.Run(() => _dataaccessLoad.ReadFrom(String.Empty)).Result;
            Assert.AreEqual(_wh.Rows, 4);
            Assert.AreEqual(_wh.Cols, 4);
            Assert.AreEqual(_wh.PodContents.Count, 1);
            Assert.AreEqual(_wh.RobotIDs.Count, 2);
            for (Int32 i = 0; i < 4; i++)
                for (Int32 j = 0; j < 4; j++)
                {
                    Assert.AreEqual(_wh[i, j], _warehouse[i, j]);
                }
        }

        [TestMethod]
        public void CheckTestEmpty()
        {
            Warehouse _wh = new Warehouse(4, 4);
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_wh), false);
        }

        [TestMethod]
        public void CheckTestNoStation()
        {
            Warehouse _wh = new Warehouse(4, 4);
            _wh.PodContents.Add(new List<Int32>() { 1 });
            _wh.RobotIDs.Add(1);
            _wh.DockCount++;
            _wh.PodCount++;
            _wh[0, 0] = FieldType.Robot;
            _wh[0, 1] = FieldType.Pod;
            _wh[0, 2] = FieldType.Dock;
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_wh), false);
        }

        [TestMethod]
        public void CheckTestNoDock()
        {
            Warehouse _wh = new Warehouse(4, 4);
            _wh.PodContents.Add(new List<Int32>() { 1 });
            _wh.StationIDs.Add(1);
            _wh.RobotIDs.Add(1);
            _wh.PodCount++;
            _wh[0, 0] = FieldType.Robot;
            _wh[0, 1] = FieldType.Station;
            _wh[0, 2] = FieldType.Pod;
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_wh), false);
        }

        [TestMethod]
        public void CheckTestNoPod()
        {
            Warehouse _wh = new Warehouse(4, 4);
            _wh.PodContents.Add(new List<Int32>() { 1 });
            _wh.StationIDs.Add(1);
            _wh.RobotIDs.Add(1);
            _wh.DockCount++;
            _wh[0, 0] = FieldType.Robot;
            _wh[0, 1] = FieldType.Station;
            _wh[0, 2] = FieldType.Dock;
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_wh), false);
        }

        [TestMethod]
        public void CheckTestNoRobot()
        {
            Warehouse _wh = new Warehouse(4, 4);
            _wh.PodContents.Add(new List<Int32>() { 1 });
            _wh.StationIDs.Add(1);
            _wh.DockCount++;
            _wh.PodCount++;
            _wh[0, 0] = FieldType.Pod;
            _wh[0, 1] = FieldType.Station;
            _wh[0, 2] = FieldType.Dock;
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_wh), false);
        }

        [TestMethod]
        public void CheckTestWrongSize()
        {
            Warehouse _whSmall = new Warehouse(1, 1);
            _whSmall.PodContents.Add(new List<Int32>() { 1 });
            _whSmall.StationIDs.Add(1);
            _whSmall.RobotIDs.Add(1);
            _whSmall.DockCount++;
            _whSmall.PodCount++;
            Warehouse _whBig = new Warehouse(500, 500);
            _whBig.PodContents.Add(new List<Int32>() { 1 });
            _whBig.StationIDs.Add(1);
            _whBig.RobotIDs.Add(1);
            _whBig.DockCount++;
            _whBig.PodCount++;
            _whBig[0, 0] = FieldType.Station;
            _whBig[2, 0] = FieldType.Dock;
            _whBig[0, 2] = FieldType.Pod;
            _whBig[2, 2] = FieldType.Robot;
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_whSmall), false);
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_whBig), false);
        }

        [TestMethod]
        public void CheckTestClosedField()
        {
            Warehouse _wh = new Warehouse(4, 4);
            _wh.PodContents.Add(new List<Int32>() { 1 });
            _wh.StationIDs.Add(1);
            _wh.RobotIDs.Add(1);
            _wh.DockCount++;
            _wh.PodCount++;
            _wh[0, 0] = FieldType.Robot;
            _wh[0, 1] = FieldType.Dock;
            _wh[1, 0] = FieldType.Pod;
            _wh[1, 1] = FieldType.Station;
            Assert.AreEqual(_dataaccessTests.CheckWarehouse(_wh), false);
        }
    }
}
