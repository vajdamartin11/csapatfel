﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace WarehouseProject.Model
{
    public enum Dir { Up, Down, Left, Right}
    public class Robot : Component
    {
        public Int32 Charge { get; set; }
        public Int32 ChargeUsed { get; set; }
        public Int32 RobotID { get; set; }
        public Dir Direction { get; set; }
        public Boolean HasWork { get; set; }
        public Boolean HasPod { get; set; }       
		public Pod Pod { get; set; }
        public Int32 ChargeStepsLeft { get; set; }
        public Stack<Point> Path { get;set; }
        public Int32 WaitedFor { get; set; }

        public Robot(Point p, Int32 ID)
        {
            _position = p;
            RobotID = ID;
            Charge = 100;
            ChargeUsed = 0;
            Direction = Dir.Left;
            HasWork = false;
            HasPod = false;
            ChargeStepsLeft = -1;
            WaitedFor = 0;
            Path = new Stack<Point>();
        }
        public void TurnRight()
        {
            if (Charge == 0) return;

            switch (Direction)
            {
                case Dir.Up:
                    Direction = Dir.Right;
                    break;
                case Dir.Right:
                    Direction = Dir.Down;
                    break;
                case Dir.Down:
                    Direction = Dir.Left;
                    break;
                case Dir.Left:
                    Direction = Dir.Up;
                    break;
            }
            Charge -= 1;
            ChargeUsed += 1;
        }
        public void TurnLeft()
        {
            if (Charge == 0) return;

            switch (Direction)
            {
                case Dir.Up:
                    Direction = Dir.Left;
                    break;
                case Dir.Left:
                    Direction = Dir.Down;
                    break;
                case Dir.Down:
                    Direction = Dir.Right;
                    break;
                case Dir.Right:
                    Direction = Dir.Up;
                    break;
            }
            Charge -= 1;
            ChargeUsed += 1;
        }
        public void GoForward()
        {
            if (Charge == 0) return;

            switch (Direction)
            {
                case Dir.Up:
                    _position.X -= 1;
                    break;
                case Dir.Left:
                    _position.Y -= 1;
                    break;
                case Dir.Down:
                    _position.X += 1;
                    break;
                case Dir.Right:
                    _position.Y += 1;
                    break;
            }
            Charge -= 1;
            ChargeUsed += 1;
        }
        public void LiftPod(Pod p)
        {
            if (Charge == 0) return;

            HasPod = true;
            Charge -= 1;
            ChargeUsed += 1;
            Pod = p;
        }
        public void UnLiftPod()
        {
            if (Charge == 0) return;

            HasPod = false;
            Charge -= 1;
            ChargeUsed += 1;
            Pod = null;
        }
    }
}