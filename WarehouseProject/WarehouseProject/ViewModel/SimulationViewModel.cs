﻿using System;
using System.Collections.ObjectModel;
using WarehouseProject.Model;
using WarehouseProject.Persistence;
using System.Drawing;
using System.Windows.Threading;

namespace WarehouseProject.ViewModel
{
    public class SimulationViewModel : ViewModelBase
    {
        private SimulationController _model;

        private Int32 _speedFactor = 1000;
        private DispatcherTimer _time;
        public Int32 Rows { get; set; }
        public Int32 Cols { get; set; }
        public String PlayPause { get; set; }
        public String Speed { get; set; }
        public Int32 RobotsCount { get { return _model.Robots.Count; } set { if (value != RobotsCount) RobotsCount = value; } }

        public ObservableCollection<PlanRobotData> RobotData { get; set; }
        public ObservableCollection<PlanField> Fields { get; set; }

        public DelegateCommand PlayPauseCommand { get; private set; }
        public DelegateCommand FasterCommand { get; private set; }
        public DelegateCommand SlowerCommand { get; private set; }
        public DelegateCommand StatusOpenCommand { get; private set; }
        public DelegateCommand QuitCommand { get; private set; }

        public event EventHandler StatusOpenClicked;
        public event EventHandler QuitClicked;

        public SimulationViewModel(SimulationController model)
        {
            _model = model;
            _model.SimulationOver += Model_SimulationOver;
            _model.SimulationAdvanced += Model_SimulationAdvanced;

            _time = new DispatcherTimer();
            _time.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            _time.Tick += new EventHandler(_model.AdvanceTime);

            PlayPauseCommand = new DelegateCommand(o => OnPlayPauseClicked());
            FasterCommand = new DelegateCommand(o => OnFasterClicked());
            SlowerCommand = new DelegateCommand(o => OnSlowerClicked());
            StatusOpenCommand = new DelegateCommand(o => OnStatusOpenClicked());
            QuitCommand = new DelegateCommand(o => OnQuitClicked());
        }

        private void Model_SimulationOver(object sender, SimulationEventArgs e)
        {
            _time.Stop();
        }

        public void Model_SimulationAdvanced(object sender, EventArgs e)
        {
            UpdateField();

            for (Int32 i = 0; i < _model.Robots.Count; i++)
            {
                string segedDir = "";
                switch (_model.Robots[i].Direction)
                {
                    case Dir.Down:
                        segedDir = "le";
                        break;
                    case Dir.Left:
                        segedDir = "bal";
                        break;
                    case Dir.Up:
                        segedDir = "fel";
                        break;
                    case Dir.Right:
                        segedDir = "jobb";
                        break;
                }
                RobotData[i].Data = Convert.ToString(_model.Robots[i].RobotID) + ". robot\nTöltöttség: " + _model.Robots[i].Charge + "\nIrány: "+ segedDir + "\nX:" + _model.Robots[i].Position.X + " - Y:" + _model.Robots[i].Position.Y;
            }

            OnPropertyChanged();
        }

        public void UpdateSimulation(Int32 rows, Int32 cols)
        {
            Fields = new ObservableCollection<PlanField>();
            Rows = rows;
            Cols = cols;
            RobotsCount = _model.Robots.Count;
            PlayPause = "Indítás";
            Speed = "Mp/lépés: 1.0";
            RobotData = new ObservableCollection<PlanRobotData>();

            for (Int32 i = 0; i < _model.Robots.Count; i++)
            {
                RobotData.Add(new PlanRobotData
                {
                    Id = _model.Robots[i].RobotID - 1,
                    Data = Convert.ToString(_model.Robots[i].RobotID) + ". robot\nTöltöttség: " + _model.Robots[i].Charge + "\nIrány: bal" + "\nX - Y"
                });
            }
            for (Int32 i = 0; i < Rows; i++)
            {
                for (Int32 j = 0; j < Cols; j++)
                {
                    Fields.Add(new PlanField
                    {
                        Value = 0,
                        X = i,
                        Y = j,
                        Number = i * cols + j,
                        Text = ""
                    });
                }
            }
            
            UpdateField();

        }

        private void UpdateField()
        {
            for (Int32 i = 0; i < Rows; i++)
            {
                for (Int32 j = 0; j < Cols; j++)
                {
                    switch (_model.Field[i, j])
                    {
                        case FieldType.Empty:
                            Fields[i * Cols + j].Value = FieldType.Empty;
                            Fields[i * Cols + j].Text = "";
                            break;
                        case FieldType.Robot:
                            Fields[i * Cols + j].Value = FieldType.Robot;
                            foreach (Robot robot in _model.Robots)
                            {
                                if (robot.Position == new Point(i, j))
                                {
                                    Fields[i * Cols + j].Text = robot.RobotID.ToString();
                                }
                            }
                            break;
                        case FieldType.Dock:
                            Fields[i * Cols + j].Value = FieldType.Dock;
                            Fields[i * Cols + j].Text = "";
                            break;
                        case FieldType.EmptyPod:
                            Fields[i * Cols + j].Value = FieldType.Pod;
                            Fields[i * Cols + j].Text = "";
                            break;
                        case FieldType.Pod:
                            Fields[i * Cols + j].Value = FieldType.Pod;
                            foreach (Pod pod in _model.Pods)
                            {
                                if (pod.Position == new Point(i, j))
                                {
                                    String _text = String.Join(" ", pod.Products);
                                    Fields[i * Cols + j].Text = _text;
                                }
                            }
                            break;
                        case FieldType.Station:
                            Fields[i * Cols + j].Value = FieldType.Station;
                            foreach (Station station in _model.Stations)
                            {
                                if (station.Position == new Point(i, j))
                                {
                                    Fields[i * Cols + j].Text = station.StationID.ToString();
                                }
                            }
                            break;
                    }
                }
            }
        }


        private void OnPlayPauseClicked()
        {
            if(PlayPause == "Indítás")
            {
                PlayPause = "Szünet";
                OnPropertyChanged("PlayPause");
                _time.Start();
            }
            else if(PlayPause == "Szünet")
            {
                PlayPause = "Indítás";
                OnPropertyChanged("PlayPause");
                _time.Stop();
            }
        }

        private void OnFasterClicked()
        {
            if (_speedFactor != 100)
            {
                _speedFactor -= 100;
                _time.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            }
            Speed = "Mp/lépés: " + Convert.ToString(_speedFactor / 1000) + "." + Convert.ToString(_speedFactor % 1000 / 100);
            OnPropertyChanged("Speed");
        }

        private void OnSlowerClicked()
        {
            if (_speedFactor != 6000)
            {
                _speedFactor += 100;
                _time.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            }
            Speed = "Mp/lépés: " + Convert.ToString(_speedFactor / 1000) + "." + Convert.ToString(_speedFactor % 1000 / 100);
            OnPropertyChanged("Speed");
        }

        private void OnStatusOpenClicked()
        {
            if (StatusOpenClicked != null)
                StatusOpenClicked(this, EventArgs.Empty);
        }

        private void OnQuitClicked()
        {
            if (QuitClicked != null)
            {
                _time.Stop();
                QuitClicked(this, EventArgs.Empty);
            }
        }
    }
}
