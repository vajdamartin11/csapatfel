﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Linq;


namespace WarehouseProject.Persistence
{
    public class FileDataAccess : IDataAccess
    {
        public Boolean CheckWarehouse(Warehouse wh)
        {
            // ha valami hiányzik/negatív darabszámú
            if (wh.DockCount <= 0 || wh.StationIDs.Count == 0 ||
                wh.RobotIDs.Count == 0 || wh.PodCount == 0)
                return false;

            // ha duplikált indexek vannak
            if (wh.RobotIDs.GroupBy(x => x).Any(y => y.Count() > 1) ||
                wh.StationIDs.GroupBy(x => x).Any(y => y.Count() > 1))
                return false;
            
            // ha van lerakó nélküli termék
            if (!wh.PodContents.All(podlist => podlist.All(prod => wh.StationIDs.Contains(prod))))
                return false;

            // a raktár mérete nem megfelelő (ha a mezők számával gond van, az betöltéskor már kiderül)
            if (wh.Rows < 3 || wh.Rows > 40 || wh.Cols < 3 || wh.Cols > 40)
                return false;
            
            // nincs teljesen körülzárt, nem üres mező
            for (Int32 i = 0; i < wh.Rows; i++)
            {
                for (Int32 j = 0; j < wh.Cols; j++)
                {
                    if (wh[i, j] == FieldType.Empty)
                        continue;

                    Int32 nonEmptyCount = 0;

                    if (i == 0 || wh[i - 1, j] != FieldType.Empty)
                        nonEmptyCount++;
                    if (i == wh.Rows - 1 || wh[i + 1, j] != FieldType.Empty)
                        nonEmptyCount++;
                    if (j == 0 || wh[i, j - 1] != FieldType.Empty)
                        nonEmptyCount++;
                    if (j == wh.Cols - 1 || wh[i, j + 1] != FieldType.Empty)
                        nonEmptyCount++;

                    if (nonEmptyCount == 4)
                        return false;
                }
            }

            return true;
        }
        public async Task<Warehouse> ReadFrom(String path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    String line = await reader.ReadLineAsync();
                    Int32[] initData = Array.ConvertAll(line.Split(' '), Int32.Parse);

                    if (initData.Length != 2)
                        throw new IOException("A fájl szerkezete nem megfelelő!");

                    Int32 rows = initData[0];
                    Int32 cols = initData[1];
                    Int32 dockCount = 0;
                    Int32 podCount = 0;

                    Warehouse wh = new Warehouse(rows, cols);
                    List<Int32> statIDs = new List<Int32>();
                    List<Int32> robotIDs = new List<Int32>();
                    List<List<Int32>> podContents = new List<List<Int32>>();

                    for (Int32 curRow = 0; curRow < rows; curRow++)
                    {
                        line = await reader.ReadLineAsync();
                        if (line == null) throw new IOException("A sorok száma nem megfelelő!");

                        String[] data = line.Split(" ");
                        if (data.Length < cols) throw new IOException("Az oszlopok száma nem megfelelő!");

                        for (Int32 curCol = 0; curCol < cols; curCol++)
                        {
                            if (data[curCol] == "0")
                                wh[curRow, curCol] = FieldType.Empty;
                            else if (data[curCol] == "1")
                            {
                                wh[curRow, curCol] = FieldType.Dock;
                                dockCount++;
                            }
                            else if (data[curCol] == "[]")
                            {
                                wh[curRow, curCol] = FieldType.EmptyPod;
                                podCount++;
                            }

                            else
                            {
                                char brace = data[curCol][0];
                                String[] data2 = data[curCol].Substring(1, data[curCol].Length - 2).Split(",");

                                if (brace == '(')
                                {
                                    wh[curRow, curCol] = FieldType.Robot;
                                    robotIDs.Add(Int32.Parse(data2[0]));
                                }
                                else if (brace == '{')
                                {
                                    wh[curRow, curCol] = FieldType.Station;
                                    statIDs.Add(Int32.Parse(data2[0]));

                                }
                                else if (brace == '[')
                                {
                                    wh[curRow, curCol] = FieldType.Pod;
                                    Int32[] products = Array.ConvertAll(data2, Int32.Parse);
                                    podContents.Add(new List<Int32>(products));
                                    podCount++;
                                }
                            }
                        }
                    }

                    wh.RobotIDs = robotIDs;
                    wh.StationIDs = statIDs;
                    wh.PodContents = podContents;
                    wh.DockCount = dockCount;
                    wh.PodCount = podCount;

                    return wh;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task SaveTo(String path, Warehouse wh)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    Int32[] firstline = { wh.Rows, wh.Cols };
                    String firstLine = String.Join(' ', firstline);
                    await writer.WriteLineAsync(firstLine);

                    Int32 robotInd = 0;
                    Int32 podInd = 0;
                    Int32 stationInd = 0;

                    for (Int32 curRow = 0; curRow < wh.Rows; curRow++)
                    {
                        StringBuilder builder = new StringBuilder();

                        for (Int32 curCol = 0; curCol < wh.Cols; curCol++)
                        {
                            switch (wh[curRow, curCol])
                            {
                                case FieldType.Empty:
                                    builder.Append("0 ");
                                    break;
                                case FieldType.Dock:
                                    builder.Append("1 ");
                                    break;
                                case FieldType.EmptyPod:
                                    builder.Append("[] ");
                                    break;
                                case FieldType.Robot:
                                    builder.Append("(" + wh.RobotIDs[robotInd] + ") ");
                                    robotInd++;
                                    break;
                                case FieldType.Station:
                                    builder.Append("{" + wh.StationIDs[stationInd] + "} ");
                                    stationInd++;
                                    break;
                                case FieldType.Pod:
                                    builder.Append("[" + String.Join(',', wh.PodContents[podInd]) + "] ");
                                    podInd++;
                                    break;
                            }
                        }

                        String res = builder.ToString();
                        res = res.Substring(0, res.Length - 1);

                        await writer.WriteLineAsync(res);
                    }
                }
            }
            catch
            {
                throw new IOException("Hiba történt a fájl írása során.");
            }
        }

        public async Task WriteLogFile(SimulationEventArgs e)
        {
            String runDir = Path.GetDirectoryName(System.Diagnostics.Process
                            .GetCurrentProcess().MainModule.FileName);
            String outName = Path.Combine(runDir,
                                  "log_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + ".txt");

            try
            {
                using (StreamWriter writer = new StreamWriter(outName))
                {
                    await writer.WriteLineAsync("Összes lépés: " + e.StepCount);
                    await writer.WriteLineAsync("Összes energia: " + e.TotalEnergy());
                    for (Int32 i = 0; i < e.EnergyPerRobot.Count; i++)
                        await writer.WriteLineAsync("Robot " + i + " energia: " + e.EnergyPerRobot[i]);
                }
            }
            catch
            {
                throw new IOException("A logfile írása sikertelen volt!");
            }
        }
    }
}
