﻿using System;
using System.Drawing;

namespace WarehouseProject.Model
{
    public class Station : Component
    {
        public Int32 StationID { get; set; }

        public Station(Point p, Int32 ID)
        {
            Position = p;
            StationID = ID;
        }
    }
}
