﻿using System;
using WarehouseProject.Persistence;

namespace WarehouseProject.ViewModel
{
    public class PlanField : ViewModelBase
    {
        private FieldType _value;
        private String _text;

        public FieldType Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    OnPropertyChanged();
                }
            }
        }

        public String Text
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    OnPropertyChanged();
                }
            }
        }

        public Int32 X { get; set; }

       
        public Int32 Y { get; set; }

        public Int32 Number { get; set; }

        public DelegateCommand Click { get; set; }
    }
}
