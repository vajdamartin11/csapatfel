﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using WarehouseProject.Model;
using WarehouseProject.Persistence;

namespace WarehouseModelTest
{
    [TestClass]
    public class PathfindingTest
    {
        private static Pathfinder pathfinder;
        private static FieldType[,] field;
        private static Stack<Point> path;
        private static Point origin;
        private static Point goal;

        [ClassInitialize]
        public static void Init(TestContext c)
        {
            field = new FieldType[6, 6];

            // init warehouse to empty
            for (Int32 i = 0; i < 6; i++)
            {
                for (Int32 j = 0; j < 6; j++)
                {
                    field[i, j] = FieldType.Empty;
                }
            }

            field[0, 0] = FieldType.Robot;
            field[0, 2] = FieldType.Dock;

            field[1, 3] = FieldType.Station;

            field[2, 0] = FieldType.Pod;
            field[2, 4] = FieldType.Robot;

            field[4, 4] = FieldType.Station;
            field[4, 5] = FieldType.Dock;

            field[5, 3] = FieldType.Robot;
            field[5, 5] = FieldType.Robot;


            pathfinder = new Pathfinder(field, 6, 6);
            path = new Stack<Point>();
        }

        [TestMethod]
        public void TestGetNextDir()
        {
            Point a = new Point(1, 1);
            Point b = new Point(1, 0);
            Point c = new Point(0, 1);

            Assert.AreEqual(Dir.Left, Pathfinder.GetNextDir(a, b));
            Assert.AreEqual(Dir.Right, Pathfinder.GetNextDir(b, a));
            Assert.AreEqual(Dir.Up, Pathfinder.GetNextDir(a, c));
            Assert.AreEqual(Dir.Down, Pathfinder.GetNextDir(c, a));
        }

        [TestMethod]
        public void TestGetTurnDir()
        {
            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Left, Dir.Up), Dir.Right);
            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Left, Dir.Down), Dir.Left);

            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Right, Dir.Up), Dir.Left);
            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Right, Dir.Down), Dir.Right);

            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Up, Dir.Right), Dir.Right);
            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Up, Dir.Left), Dir.Left);

            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Down, Dir.Right), Dir.Left);
            Assert.AreEqual(Pathfinder.GetTurnDir(Dir.Down, Dir.Left), Dir.Right);
        }

        [TestMethod]
        public void TestShortestPathPodCarrying()
        {
            origin = new Point(0, 0);
            goal = new Point(3, 0);
            Point podPoint = new Point(2, 0);

            // not carrying -> can go under
            pathfinder.ShortestPath(origin, goal, Dir.Down, false, path);
            Assert.IsTrue(path.Contains(podPoint));

            // carrying -> can't go under
            pathfinder.ShortestPath(origin, goal, Dir.Down, true, path);
            Assert.IsFalse(path.Contains(podPoint));
        }

        [TestMethod]
        public void TestShortestPathNoPath()
        {
            origin = new Point(5, 5);
            goal = new Point(5, 2);

            Assert.AreEqual(-1, pathfinder.ShortestPath(origin, goal, Dir.Left));
        }

        [TestMethod]
        public void TestShortestPathObstacleAvoidance()
        {
            origin = new Point(0, 0);
            goal = new Point(0, 5);

            Point dockPoint = new Point(0, 2);
            Point stationPoint = new Point(1, 3);
            Point robotPoint = new Point(2, 4);

            pathfinder.ShortestPath(origin, goal, Dir.Right, false, path);

            // can't step on dock/robot/station if that's not the goal
            Assert.IsFalse(path.Contains(dockPoint));
            Assert.IsFalse(path.Contains(stationPoint));
            Assert.IsFalse(path.Contains(robotPoint));

            // path should contain dock/station if that's the goal
            pathfinder.ShortestPath(origin, dockPoint, Dir.Right, false, path);
            Assert.IsTrue(path.Contains(dockPoint));

            pathfinder.ShortestPath(origin, stationPoint, Dir.Right, false, path);
            Assert.IsTrue(path.Contains(stationPoint));
        }

        [TestMethod]
        public void TestShortestPathLength()
        {
            origin = new Point(0, 0);
            goal = new Point(0, 1);

            // 1 away, direction shouldn't affect path
            pathfinder.ShortestPath(origin, goal, Dir.Left, false, path);
            Assert.AreEqual(1, path.Count);
            pathfinder.ShortestPath(origin, goal, Dir.Up, false, path);
            Assert.AreEqual(1, path.Count);

            // path length when carrying/going under pod
            goal = new Point(3, 0);
            pathfinder.ShortestPath(origin, goal, Dir.Left, false, path);
            Assert.AreEqual(3, path.Count);

            pathfinder.ShortestPath(origin, goal, Dir.Left, true, path);
            Assert.AreEqual(5, path.Count);

            // longer, avoiding obstacles
            goal = new Point(0, 5);
            pathfinder.ShortestPath(origin, goal, Dir.Left, false, path);
            Assert.AreEqual(11, path.Count);
        }

        [TestMethod]
        public void TestShortestPathEnergyReturned()
        {
            origin = new Point(0, 0);
            goal = new Point(0, 1);

            // 1 away, turn count different
            Assert.AreEqual(1, pathfinder.ShortestPath(origin, goal, Dir.Right));
            Assert.AreEqual(2, pathfinder.ShortestPath(origin, goal, Dir.Down));
            Assert.AreEqual(2, pathfinder.ShortestPath(origin, goal, Dir.Up));
            Assert.AreEqual(3, pathfinder.ShortestPath(origin, goal, Dir.Left));

            // carrying/going under pod path
            goal = new Point(3, 0);
            Assert.AreEqual(3, pathfinder.ShortestPath(origin, goal, Dir.Down));
            Assert.AreEqual(8, pathfinder.ShortestPath(origin, goal, Dir.Down, true));

            // longer, avoiding obstacles with many turns
            goal = new Point(0, 5);
            Assert.AreEqual(14, pathfinder.ShortestPath(origin, goal, Dir.Left));
        }
    }
}
