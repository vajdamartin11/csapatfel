﻿using System;
using WarehouseProject.Persistence;

namespace WarehouseProject.ViewModel
{
    public class LoadEventArgs : EventArgs
    {
        public Warehouse Warehouse { get; set; }

        public LoadEventArgs(Warehouse warehouse)
        {
            Warehouse = warehouse;
        }
    }
}
