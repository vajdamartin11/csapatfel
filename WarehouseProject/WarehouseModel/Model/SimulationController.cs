﻿using System;
using System.Collections.Generic;
using System.Drawing;
using WarehouseProject.Persistence;

namespace WarehouseProject.Model
{
    public class SimulationController
    {
        private List<FieldType> _underRobot;
        private Int32 _stepCount;
        private FieldType[,] _field;
        private Pathfinder _pathfinder;
        private Int32 _maxCharge;

        public FieldType[,] Field { get { return _field; } set { _field = value; } }
        public List<Robot> Robots { get; set; }
        public List<Dock> Docks { get; set; }
        public List<Station> Stations { get; set; }
        public List<Pod> Pods { get; set; }

        public EventHandler SimulationAdvanced;
        public EventHandler<SimulationEventArgs> SimulationOver;

        public SimulationController() { }

        public void NewSimulation(Warehouse wh)
        {
            Robots = new List<Robot>();
            Docks = new List<Dock>();
            Stations = new List<Station>();
            Pods = new List<Pod>();

            _field = new FieldType[wh.Rows, wh.Cols];
            _underRobot = new List<FieldType>();
            _pathfinder = new Pathfinder(_field, wh.Rows, wh.Cols);
            _maxCharge = 100;
            _stepCount = 0;

            Int32 _stationIndexer = 0;
            Int32 _robotIndexer = 0;
            Int32 _podIndexer = 0;

            for (Int32 i = 0; i < wh.Rows; i++) 
            {
                for (Int32 j = 0; j < wh.Cols; j++)
                {
                    _field[i, j] = wh[i, j];
                    switch (wh[i, j])
                    {
                        case FieldType.Robot:
                            Robots.Add(new Robot(new Point(i, j), wh.RobotIDs[_robotIndexer++]));
                            Robots[_robotIndexer - 1].Charge = _maxCharge;
                            _underRobot.Add(FieldType.Empty);
                            break;
                        case FieldType.Dock:
                            Docks.Add(new Dock(new Point(i, j)));
                            break;
                        case FieldType.EmptyPod:
                            Pods.Add(new Pod(new Point(i, j)));
                            break;
                        case FieldType.Pod:
                            Pods.Add(new Pod(new Point(i, j), wh.PodContents[_podIndexer++]));
                            break;
                        case FieldType.Station:
                            Stations.Add(new Station(new Point(i, j), wh.StationIDs[_stationIndexer++]));
                            break;
                    }
                }
            }
        }
        public void AdvanceTime(object sender, EventArgs e)
        {
            MainController();

            OnSimulationAdvanced();

            if (CheckIsOver())
                OnSimulationOver();
        }

        public void MainController()
        {
            foreach ( Robot robot in Robots)
            {
                if (!robot.HasWork && robot.HasPod)
                {
                    Int32 prodToDrop = HasEnoughEnergyForProduct(robot, robot.Pod);
                    if (prodToDrop <= 0)
                    {
                        GoToPodPosition(robot);
                    }
                    else
                    {
                        GoToStation(robot, prodToDrop);
                    }
                    robot.HasWork = true;
                }
                else if(!robot.HasWork)
                {
                    Boolean foundGoalPod = false;
                    Pod maxPod = null;
                    Int32 energy = -1;

                    FindBestPod(robot, ref foundGoalPod, ref maxPod, ref energy);

                    if (foundGoalPod && energy <= robot.Charge)
                    {
                        _pathfinder.ShortestPath(robot.Position, maxPod.Position, robot.Direction, robot.HasPod, robot.Path);
                        maxPod.Taken = true;
                        robot.HasWork = true;
                    }
                    else
                    {
                        GoToClosestDock(robot);
                    }
                }

                MovesController(robot);
            }

            _stepCount++;
        }

        private void GoToClosestDock(Robot robot)
        {
            Dock closestDock = Docks[0];
            Int32 energy = _pathfinder.ShortestPath(robot.Position, closestDock.Position, robot.Direction, robot.HasPod, robot.Path);
            Int32 tempEnergy;

            foreach (Dock dock in Docks)
            {
                tempEnergy = _pathfinder.ShortestPath(robot.Position, dock.Position, robot.Direction, robot.HasPod, robot.Path);
                if (tempEnergy < energy)
                {
                    closestDock = dock;
                    energy = tempEnergy;
                }
            }

            _pathfinder.ShortestPath(robot.Position, closestDock.Position, robot.Direction, robot.HasPod, robot.Path);
            robot.HasWork = true;
        }

        private void FindBestPod(Robot robot, ref bool foundGoalPod, ref Pod maxPod, ref int energy)
        {
            foreach (Pod pod in Pods)
            {
                if (pod.Taken) continue;

                if (maxPod == null)
                {
                    maxPod = pod;
                    energy = _pathfinder.ShortestPath(robot.Position, maxPod.Position, robot.Direction, robot.HasPod, robot.Path);
                }

                Int32 energyNeeded = _pathfinder.ShortestPath(robot.Position, pod.Position, robot.Direction, robot.HasPod) + 1; // lift
                Int32 tempEnergy = energyNeeded;

                if (HasEnoughEnergyForProduct(robot, pod, energyNeeded) > 0)
                {
                    foundGoalPod = true;

                    if (maxPod.Products.Count < pod.Products.Count)
                    {
                        foundGoalPod = true;
                        maxPod = pod;
                        energy = _pathfinder.ShortestPath(robot.Position, maxPod.Position, robot.Direction, robot.HasPod, robot.Path);
                    }
                    else if (maxPod.Products.Count == pod.Products.Count)
                    {
                        Int32 tempEner = _pathfinder.ShortestPath(robot.Position, pod.Position, robot.Direction, robot.HasPod, robot.Path);
                        if (energy > tempEner)
                        {
                            foundGoalPod = true;
                            maxPod = pod;
                            energy = tempEner;
                        }
                    }
                }
            }
        }

        private void GoToPodPosition(Robot robot)
        {
            _pathfinder.ShortestPath(robot.Position, robot.Pod.OriginalPos, robot.Direction, robot.HasPod, robot.Path);
        }

        private void GoToStation(Robot robot, int prodToDrop)
        {
            Int32 i;
            for (i = 0; i < Stations.Count; i++)
            {
                if (Stations[i].StationID == prodToDrop)
                    break;
            }

            _pathfinder.ShortestPath(robot.Position, Stations[i].Position, robot.Direction, robot.HasPod, robot.Path);
        }

        // Returns first prod number that can be dropped (with enough left to dock), -1 if none, 0 if pod empty.
        private Int32 HasEnoughEnergyForProduct(Robot robot, Pod pod, Int32 initEnergyNeeded = 0)
        {
            if (pod.Products.Count == 0) return 0;

            // oda + min 1 termék + vissza + töltő
            foreach (Int32 product in pod.Products)
            {
                Int32 tempEnergy = initEnergyNeeded;
                tempEnergy += _pathfinder.ShortestPath(pod.Position, Stations[product - 1].Position, robot.Direction, true) + 2;
                tempEnergy += _pathfinder.ShortestPath(Stations[product - 1].Position, pod.OriginalPos, robot.Direction, true) + 1 + 2; // unlift
                foreach (Dock dock in Docks)
                {
                    Int32 tempEnergy2 = tempEnergy;
                    tempEnergy2 += _pathfinder.ShortestPath(pod.Position, dock.Position, robot.Direction, false) + 1 + 2; //start charge

                    if (tempEnergy2 <= robot.Charge)
                    {
                        return product;
                    }
                }
            }

            return -1;
        }

        private void MovesController(Robot robot)
        {
            if(robot.Path.Count == 0)
            {
                Boolean hasWork = false;

                if (_underRobot[robot.RobotID - 1] == FieldType.Station)
                {
                    DropCurrentProduct(robot);
                }
                else if (_underRobot[robot.RobotID - 1] == FieldType.Pod && !robot.HasPod)
                {
                    LiftCurrentPod(robot);
                }
                else if (_underRobot[robot.RobotID - 1] == FieldType.Empty && robot.HasPod)
                {
                    UnliftCurrentPod(robot);
                }
                else
                {
                    hasWork = ChargeRobot(robot);
                }

                robot.HasWork = hasWork;
            }
            else
            {
                Point next = robot.Path.Peek();
                Dir nextDir = Pathfinder.GetNextDir(robot.Position, next);
                if (nextDir != robot.Direction)
                {
                    if (Pathfinder.GetTurnDir(robot.Direction, nextDir) == Dir.Left)
                        robot.TurnLeft();
                    else
                        robot.TurnRight();
                }
                else
                {
                    if (!RecalculatedStuckRobot(robot, next))
                    {
                        MoveRobotForward(robot);
                    }
                }
            }
        }

        private void MoveRobotForward(Robot robot)
        {
            _field[robot.Position.X, robot.Position.Y] = _underRobot[robot.RobotID - 1];

            robot.GoForward();
            robot.Path.Pop();

            if (robot.HasPod)
                robot.Pod.Position = robot.Position;

            FieldType steppedOn = _field[robot.Position.X, robot.Position.Y];
            _underRobot[robot.RobotID - 1] = steppedOn;

            // robot not visible when under pod, otherwise fieldtype should be robot
            if (steppedOn == FieldType.Pod || steppedOn == FieldType.EmptyPod || robot.HasPod)
            {
                _field[robot.Position.X, robot.Position.Y] = FieldType.Pod;
            }
            else
            {
                _field[robot.Position.X, robot.Position.Y] = FieldType.Robot;
            }
        }

        private Boolean RecalculatedStuckRobot(Robot robot, Point next)
        {
            foreach (Robot r in Robots)
            {
                if (robot.RobotID == r.RobotID) continue;
                if (r.Position == next)
                {
                    robot.WaitedFor++;
                    if (robot.WaitedFor > 5)
                    {
                        if (robot.HasPod)
                            GoToPodPosition(robot);
                        else
                        {
                            robot.HasWork = false;
                            ResetPods();
                        }

                        robot.WaitedFor = 0;
                    }
                    return true;
                }
            }

            return false;
        }

        private void ResetPods()
        {
            foreach (Pod p in Pods)
            {
                if (p.Taken)
                {
                    Boolean hasRobot = false;
                    foreach (Robot rob in Robots)
                    {
                        if (rob.Position == p.Position)
                        {
                            hasRobot = true;
                            break;
                        }
                    }
                    if (hasRobot)
                        break;
                    // no robot at pod pos, but still taken
                    p.Taken = false;
                }
            }
        }

        private Boolean ChargeRobot(Robot robot)
        {
            if (robot.ChargeStepsLeft == -1)
            {
                robot.ChargeStepsLeft = 5;
                robot.ChargeUsed++;
                robot.Charge--;
                return true;
            }
            if (robot.ChargeStepsLeft > 1)
            {
                robot.ChargeStepsLeft--;
                return true;
            }

            robot.Charge = _maxCharge;
            robot.ChargeStepsLeft = -1;

            return false;
        }

        private void UnliftCurrentPod(Robot robot)
        {
            FieldType podType = (robot.Pod.Products.Count == 0) ? FieldType.EmptyPod : FieldType.Pod;
            _underRobot[robot.RobotID - 1] = podType;
            robot.Pod.Taken = false;
            robot.UnLiftPod();
        }

        private void LiftCurrentPod(Robot robot)
        {
            Pod pod = null;
            foreach (Pod p in Pods)
            {
                if (p.Position == robot.Position)
                    pod = p;
            }
            robot.LiftPod(pod);
            _underRobot[robot.RobotID - 1] = FieldType.Empty;
        }

        private void DropCurrentProduct(Robot robot)
        {
            Int32 prod = -1;
            foreach (Station st in Stations)
            {
                if (st.Position == robot.Position)
                    prod = st.StationID;
            }
            robot.Pod.RemoveProduct(prod);
        }

        private Boolean CheckIsOver()
        {
            // van még termék vagy nincs minden polc a helyén
            foreach (Pod pod in Pods)
            {
                if (pod.Position != pod.OriginalPos)
                    return false;
                if (pod.Products.Count > 0)
                    return false;
            }

            return true;
        }
        
        private void OnSimulationAdvanced()
        {
            if (SimulationAdvanced != null)
                SimulationAdvanced(this, EventArgs.Empty);
        }
        private void OnSimulationOver()
        {
            if (SimulationOver != null)
                SimulationOver(this, new SimulationEventArgs(_stepCount, GetRobotsEnergyUsed()));
        }

        private List<Int32> GetRobotsEnergyUsed()
        {
            List<Int32> l = new List<Int32>();
            foreach (Robot r in Robots)
                l.Add(r.ChargeUsed);

            return l;
        }
    }
}
