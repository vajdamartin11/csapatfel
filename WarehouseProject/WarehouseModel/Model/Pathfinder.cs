﻿using System;
using System.Collections.Generic;
using System.Drawing;
using WarehouseProject.Persistence;

namespace WarehouseProject.Model
{
    public class Pathfinder
    {
        private readonly Int32 X_SIZE;
        private readonly Int32 Y_SIZE;


        private FieldType[,] _field;
        private Dictionary<Point, Point> _parentsDict = new Dictionary<Point, Point>();
        private Dictionary<Point, Int32> _distanceDict = new Dictionary<Point, Int32>();
        private Dictionary<Point, Int32> _guessDict = new Dictionary<Point, Int32>();
        PrQueue _prQ = new PrQueue();


        public Pathfinder(FieldType[,] f, Int32 rows, Int32 cols)
        {
            _field = f;
            X_SIZE = rows;
            Y_SIZE = cols;
        }


        public static Dir GetNextDir(Point cur, Point next)
        {
            // X changed
            Int32 delta = next.X - cur.X;
            if (delta == 1) return Dir.Down;
            if (delta == -1) return Dir.Up;

            // Y changed
            delta = next.Y - cur.Y;
            if (delta == 1) return Dir.Right;
            if (delta == -1) return Dir.Left;

            return Dir.Left; // default
        }

        public static Dir GetTurnDir(Dir cur, Dir next)
        {
            if (cur == next) return 0;

            switch (cur)
            {
                case Dir.Down: return (next == Dir.Left) ? Dir.Right : Dir.Left;
                case Dir.Up: return (next == Dir.Left) ? Dir.Left : Dir.Right;
                case Dir.Left: return (next == Dir.Up) ? Dir.Right : Dir.Left;
                case Dir.Right: return (next == Dir.Up) ? Dir.Left : Dir.Right;
                default: return 0;
            }
        }


        public Int32 ShortestPath(Point from, Point to, Dir startDir, Boolean carrying = false, Stack<Point> path = null)
        {
            if (from == to) return -1;

            _prQ.Clear();
            _parentsDict.Clear();
            _distanceDict.Clear();
            _guessDict.Clear();

            _prQ.Enqueue(new Node(from, 0, startDir));
            _distanceDict[from] = 0;

            while (!_prQ.IsEmpty())
            {
                Node curNode = _prQ.Dequeue();

                // reached goal
                if (curNode.point == to)
                {
                    if (path == null)
                        path = new Stack<Point>();
                    else
                        path.Clear();

                    Point curPoint = curNode.point;
                    while (curPoint != from)
                    {
                        path.Push(curPoint);
                        curPoint = _parentsDict[curPoint];
                    }

                    return GetEnergyCount(CopyStack(path), from, startDir);
                }


                foreach (Node node in GetNodeNeighbours(curNode, carrying, to))
                {
                    if (!_distanceDict.ContainsKey(node.point))
                        _distanceDict[node.point] = Int32.MaxValue;

                    Int32 turnCount = GetTurnCount(curNode.dir, node.dir);
                    Int32 distance = _distanceDict[curNode.point] + 1 + turnCount;

                    // found better distance
                    if (_distanceDict[node.point] > distance)
                    {
                        _distanceDict[node.point] = distance;
                        _parentsDict[node.point] = curNode.point;
                        _guessDict[node.point] = 2 * distance + ManhDist(node.point, to);

                        if (!_prQ.ContainsPoint(node.point))
                            _prQ.Enqueue(new Node(node.point, _guessDict[node.point], node.dir, turnCount));
                    }
                }
            }

            return -1;
        }


        private static Int32 ManhDist(Point start, Point goal)
        {
            return Math.Abs(goal.X - start.X) + Math.Abs(goal.Y - start.Y);
        }

        private static Stack<Point> CopyStack(Stack<Point> path)
        {
            return new Stack<Point>(new Stack<Point>(path));
        }

        private static Int32 GetEnergyCount(Stack<Point> path, Point startPoint, Dir startDir)
        {
            // process first step + init variables
            Point prev = startPoint;
            Point cur = path.Pop();

            Dir prevDir = startDir;
            Dir curDir = GetNextDir(prev, cur);

            Int32 energyCount = 1 + GetTurnCount(prevDir, curDir);

            // process the rest of the steps
            while (path.Count > 0)
            {
                prev = cur;
                cur = path.Pop();

                prevDir = curDir;
                curDir = GetNextDir(prev, cur);

                energyCount += 1 + GetTurnCount(prevDir, curDir);
            }

            return energyCount;
        }

        private static Int32 GetTurnCount(Dir cur, Dir next)
        {
            if (cur == next) return 0;

            switch (cur)
            {
                case Dir.Down: return (next == Dir.Up) ? 2 : 1;
                case Dir.Up: return (next == Dir.Down) ? 2 : 1;
                case Dir.Left: return (next == Dir.Right) ? 2 : 1;
                case Dir.Right: return (next == Dir.Left) ? 2 : 1;
                default: return 0;
            }
        }


        private Boolean IsOKField(Point point, Boolean carrying, Point goal)
        {
            if (IsOutOfBounds(point))
                return false;

            Boolean isWalkable = _field[point.X, point.Y] != FieldType.Robot &&
                              _field[point.X, point.Y] != FieldType.Dock &&
                              _field[point.X, point.Y] != FieldType.Station;

            // can't go under other pods if already carrying one
            if (carrying)
                isWalkable = isWalkable && _field[point.X, point.Y] != FieldType.Pod &&
                             _field[point.X, point.Y] != FieldType.EmptyPod;

            return (isWalkable || point == goal);
        }

        private Boolean IsOutOfBounds(Point point)
        {
            return !(point.X >= 0 && point.Y >= 0 && point.X < X_SIZE && point.Y < Y_SIZE);
        }

        private IEnumerable<Node> GetNodeNeighbours(Node origin, Boolean carrying, Point goal)
        {
            List<Node> neighbours = new List<Node>();
            Point origPoint = origin.point;
            Dir nextDir;

            Point[] neighbourPoints = {
                new Point(origPoint.X, origPoint.Y - 1),
                new Point(origPoint.X, origPoint.Y + 1),
                new Point(origPoint.X - 1, origPoint.Y),
                new Point(origPoint.X + 1, origPoint.Y)
            };

            foreach (Point newPoint in neighbourPoints)
            {
                nextDir = GetNextDir(origPoint, newPoint);
                if (IsOKField(newPoint, carrying, goal))
                    neighbours.Add(new Node(newPoint, 0, nextDir, GetTurnCount(origin.dir, nextDir)));
            }

            return neighbours;
        }


        // Node type used in the pathfinding algorithm.
        private struct Node : IComparable<Node>
        {
            public Int32 distGuess;
            public Int32 turnCount;
            public Point point;
            public Dir dir;

            public Node(Point p = default(Point), Int32 dGuess = Int32.MaxValue, Dir d = Dir.Left, Int32 t = 0)
            {
                distGuess = dGuess;
                point = p;
                dir = d;
                turnCount = t;
            }

            // first distance, next turns are used to compare
            public Int32 CompareTo(Node other)
            {
                if (distGuess - other.distGuess == 0)
                    return turnCount - other.turnCount;

                return distGuess - other.distGuess;
            }
        }

        // Minimum priority queue, smallest node gets removed first (implemented using a list).
        private class PrQueue
        {
            private List<Node> list = new List<Node>();

            public void Enqueue(Node elem)
            {
                if (list.Count == 0)
                {
                    list.Add(elem);
                    return;
                }

                Int32 ind = list.FindLastIndex(t => t.CompareTo(elem) >= 0);
                list.Insert(ind + 1, elem);
            }

            public Node Dequeue()
            {
                if (list.Count == 0)
                    return default(Node);

                Int32 ind = list.Count - 1;
                Node elem = list[ind];
                list.RemoveAt(ind);

                return elem;
            }

            public void Clear() => list.Clear();

            public Boolean IsEmpty() => list.Count == 0;

            public Boolean ContainsPoint(Point p)
            {
                return list.FindIndex(e => e.point == p) != -1;
            }
        }
    }
}
