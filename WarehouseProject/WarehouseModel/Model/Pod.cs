﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace WarehouseProject.Model
{
    public class Pod : Component
    {
        public List<Int32> Products { get; set; }
        public Point OriginalPos { get; set; }

        public Boolean Taken { get; set; }

        public Pod(Point p)
        {
            Position = p;
            OriginalPos = p;
            Products = new List<Int32>();
            Taken = false;
        }
        public Pod(Point p,List<Int32> products)
        {
            Position = p;
            OriginalPos = p;
            Products = products;
            Taken = false;
        }

        public void AddProduct(Int32 product)
        {
            Products.Add(product);
        }

        public void RemoveProduct(Int32 product)
        {
            Products.Remove(product);
        }

    }
}
