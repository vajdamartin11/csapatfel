﻿using System;
using System.Collections.Generic;

namespace WarehouseProject.Persistence
{
    public enum FieldType { Empty, Robot, Dock, EmptyPod, Pod, Station, SelectedPod, OKField , SelectedOKField}
    public class Warehouse
    {
        private FieldType[,] _plan;
        private Int32 _rows = 0;
        private Int32 _cols = 0;
        private Int32 _docks = 0;

        public Int32 PodCount { get; set; }

        public FieldType this[Int32 x, Int32 y] 
        { 
            get 
            {
                if (x < 0 || x >= _plan.GetLength(0))
                    throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
                if (y < 0 || y >= _plan.GetLength(1))
                    throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");

                return _plan[x, y];
            }
            set
            {
                if (x < 0 || x >= _plan.GetLength(0))
                    throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
                if (y < 0 || y >= _plan.GetLength(1))
                    throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");

                _plan[x, y] = value;
            }
        }

        public Int32 Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
        public Int32 Cols
        {
            get { return _cols; }
            set { _cols = value; }
        }

        public Int32 DockCount
        {
            get { return _docks; }
            set { _docks = value; }
        }

        public List<Int32> StationIDs
        {
            get;
            set;
        } = new List<Int32>();

        public List<Int32> RobotIDs
        {
            get;
            set;
        } = new List<Int32>();

        public List<List<Int32>> PodContents
        {
            get;
            set;
        } = new List<List<Int32>>();

        public Warehouse(Int32 rows, Int32 cols)
        {
            _rows = rows;
            _cols = cols;
            _plan = new FieldType[_rows, _cols];
        }
    }
}
